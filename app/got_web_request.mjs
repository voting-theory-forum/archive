/* Got a request from the WWW. */

let route_html, serve_file, heartbeep, route_json;

const gotRequest = function (req, res) {
  // We have gotten a request over the Web.
  // Take it into account and react appropriately.
  const remoteaddress = req.connection.remoteAddress;
  // Usually would be our own address,
  // because nginx has forwarded the request.
  // But if we haven't firewalled off the port I'm listening
  // to, a hostile probe could work its way directly to us.
  const {method, url, headers} = req;
  const believed_origin =
    headers['x-real-ip'] || remoteaddress;
    // Weak; could be spoofed.
  const reqdescr = believed_origin + " " + method +
    " " + url;
  console.log(reqdescr);
  /* If I were doing this over again from scratch, I would not use the `URL`
    object. I would quasi-parse the URI myself.
  */
  let req_a_url;
  try {
    req_a_url = new URL(url, "https://votingtheory.org");
  } catch (err) {
    res.setHeader("Content-Type", "text/plain; charset=utf-8");
    res.writeHead(404); /* not found */
    res.end(err.toString());
    return
  }
  const {pathname} = req_a_url;
  if ("/lub" == pathname) return heartbeep.lub(req, res);
  const spec = {req, res, reqdescr, req_a_url, pathname, method};

  if (/\.json$/.test(pathname))
    setTimeout(route_json.gotReq, 0, spec);
  else if (/\.(js)|(mjs)$/.test(pathname))
    setTimeout(serve_file.gotReq, 0, spec);
  else if (/\.css$/.test(pathname)) {
    spec.type = "text/css; charset=utf-8";
    setTimeout(serve_file.gotReq, 0, spec)
  } else if (/\.(jpg)|(jpeg)$/.test(pathname)) {
    spec.type = "image/jpeg";
    setTimeout(serve_file.gotReq, 0, spec)
  } else if (/\.png$/.test(pathname)) {
    spec.type = "image/png";
    setTimeout(serve_file.gotReq, 0, spec)
  } else if (/\/robots.txt$/.test(pathname)) {
    res.setHeader("Content-Type", "text/plain; charset=utf-8");
    res.setHeader("Cache-Control", "public, max-age=604800, immutable");
    res.writeHead(200);
    res.end("Host: votingtheory.org\n")
  } else if (/.txt$/.test(pathname)) {
    // Other *.txt request, not robots.txt
    res.setHeader("Content-Type", "text/plain; charset=utf-8");
    res.writeHead(404); // not found
    res.end("404 Not Found\n")
  } else if (/.ico$/.test(pathname)) {
    // Treat any *.ico request as though it were
    // favicon.ico.
    let newspec = {};
    newspec.cacheControlOverride = "public, max-age=604800, immutable";
    Object.assign(newspec, spec);
    newspec.pathname = "/img/logo-of-pallor.png";
    newspec.type = "image/png";
    setTimeout(serve_file.gotReq, 0, newspec)
  } else setTimeout(route_html.go, 0, spec) 
};

const asInstalled = async function (env) {
  const {mgr} = env;
  route_html = await mgr.find("app/html.mjs");
  route_json = await mgr.find("app/json.mjs");
  serve_file = await mgr.find("app/serve_file.mjs");
  heartbeep  = await mgr.find("app/heartbeep.mjs")
  return gotRequest
};

export {asInstalled}
