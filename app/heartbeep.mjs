// server
export async function asInstalled (env) {
  const {mgr} = env;
  if (typeof globalThis.app != typeof {})
    globalThis.app = {};
  let self = Object.create(beh);
  globalThis.app.heartbeep = self;
  return self
};
function hit () {
  // Called for side effect of letting me know the app is
  // getting a request even if it is for another module
  // to interpret and respond to.
  // Use the wall-clock time of these events to maintain
  // a notion of how busy the app is with being peppered
  // with requests.
  this.prev = this.last;
  this.last = Date.now();
  return undefined
};
function lub(req, res) {
  // Respond to a heartbeep request from a browser.
  res.setHeader("Content-Type", "application/json");
  res.end(JSON.stringify({
    verb: 'dub',
    recommendedPeriod: 1000 * 600, // 10 min.
    serverVersion: globalThis.app.server_version,
  }))
};
var beh = {hit, lub}
