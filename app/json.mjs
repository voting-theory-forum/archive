// Handle the requests for data to be returned in JSON.

let {lib, app} = globalThis;
let ref_registry, RefRegistry;
let collections_by_pathname = {};
let lvar, play, ohPromiseMe;
let nil, Cons;
let s = {}; // for "script", for some things belonging to the present script.
let {mgr} = app;
let use_db, import_utls, import_lvar, import_listlib;

export const asInstalled = async function () {
  app.ref_registry = ref_registry = RefRegistry.new();
  await Promise.all([import_utls(), import_lvar(), use_db(), import_listlib()]);
  let public_gotReq = function () {
    return s.gotReq.apply(s, arguments);
  };
  app.route_json = {debug: s, gotReq: public_gotReq};
  return {gotReq: public_gotReq}
}

const coll_names = ["categories", "topics", "posts", "users"];

use_db = async function () {
  await mgr.find("app/db.mjs");
  coll_names.forEach( n => {
    const pathname = `/${n}.json`;
    collections_by_pathname[pathname] = app[n]
  });
};
import_utls = async function () {
  await mgr.find("lib/pgm.mjs");
  play = lib.play;
  ohPromiseMe = lib.ohPromiseMe
};
import_lvar = async function () {
  lvar = await mgr.find("lib/lvar.mjs");
};
import_listlib = async function () {
  let listlib = await mgr.find("lib/list.mjs");
  nil = listlib.nil;
  Cons = listlib.Cons
};

s.listFromCursor = cur => {
  let t = lib.lvar.new();
  t.onCancel(() => cur.close());
  let resolution = lib.ohPromiseMe(t.reject);
  t.onDemand( () => lib.play( function* () {
    if (yield resolution(() => cur.hasNext())) {
      let it = Cons.new();
      it.car = yield resolution(() => cur.next());
      it.cdr = s.listFromCursor(cur);
      t.resolve(it)
    } else t.resolve(nil)
  }));
  return t.ask
};

// Maintain some referents to which clients can refer.
// These usually represent lists of records.
// Implementation may use a database cursor.

RefRegistry = {};
RefRegistry.beh = {};
RefRegistry.new = function () {
  let it = Object.create(this.beh);
  it.by_name = {};
  it.ct = 0;
  return it
};
RefRegistry.beh.Ref = {};
RefRegistry.beh.Ref.bye = function () {
  this.lvar.cancel();
  clearTimeout(this.timer);
  --this.owner.ct;
  delete this.owner.by_name[this.name];
  return true
};
RefRegistry.beh.new_ref = function () {
  let it = Object.create(this.Ref);
  while (true) {
    it.name = "/ref/" + Math.random() + ".json";
    if (! this.by_name[it.name]) break;
  };
  it.timer = setTimeout(it.bye.bind(it), 1000 * 60 * 10);
  it.owner = this;
  this.ct++;
  return this.by_name[it.name] = it
};
s.new_token_for = an_lvar => {
  let mem = ref_registry.new_ref();
  mem.lvar = an_lvar;
  return mem.name
};

s.getUgly = () => ({
  serverVersion: app.server_version,
  recommendedPeriod: 1000 * 600, // 10 min.
  refCt: ref_registry.ct
});

s.present = bad => abstr => good => lib.play( function* () {
  // From abstract values, whip up a serializable representation for passing
  // over the wire.
  let crawl = abstr;
  while (crawl.isLogicalVariable) crawl = yield crawl.dereference(bad);
  if (crawl.isNil) good(null);
  else if (crawl.isCons) good({
    isCons:   true,
    isList:   true,
    isStruct: true,
    car: {isLiteral: true, value: crawl.car},
    cdr: {isReference: true, URI: s.new_token_for(crawl.cdr)}
  }); else good(crawl)
});

s.gotReq = spec => lib.play( function* () { // got Web request.
  let {res, req, req_a_url, pathname, method} = spec;
  res.setHeader("Content-Type", "application/json; charset=utf-8");
  let good = result => {
    res.end(JSON.stringify({good: result, ugly: s.getUgly()}));
  };
  let bad = how => {
    console.error(how);
    res.end(JSON.stringify({bad: how,     ugly: s.getUgly()}));
  };
  let concrete = s.present(bad);
  let coll, ref;
  if (coll = collections_by_pathname[pathname]) {
    let args;
    const args_txt = req_a_url.searchParams.get('find');
    if (args_txt) {
      try {
        args = JSON.parse(args_txt);
      } catch {
        return bad(
          `The argument to the 'find' parameter doesn't look like good JSON.`
        )
      }
    } else {
      args = [];
    };
    let cur = coll.find.apply(coll, args);
    return concrete(s.listFromCursor(cur))(good)
  } else if (ref = ref_registry.by_name[pathname]) {
    if ('DELETE' === method) {
      ref.bye();
      return good(true)
    } else return concrete(ref.lvar)(good)
  } else if ("/nop.json" === pathname) {
    return good(null);
  } else {
    const protest = `JSON branch didn't recognize ${req.url}`;
    // res.writeHead(404); // not found
    console.log(protest);
    bad(protest)
  }
})

