/*
  Map in both directions between concrete and abstract
  identifiers of the pages that this site can show.

  Basic page index, because:
  basic -- does not JS-reference any code and does not consult the database.
  page  -- is only about the pages people can see.
  index -- distinguishes valid from invalid locator strings.

  Most of the information here takes effect in the front end. However, the
  back end needs this in order to know whether to return status 404 Not Found
  or 200 OK. So this file is linked into both source areas.
*/

let s = {};

export const asInstalled = async function () {
  /*
    No dependencies here. This module is just a self-contained mapping between
    two forms of symbolism.
  */
  return app.basicPageIndex = s
};

s.abstract = concrete => {
  let t = {};
  t[404] = reason => ({ [0]: 404, reason:
    `${JSON.stringify(concrete)}: ${reason}`
  });
  /*
    Asking a document element for its `href` can result
    in a URI that includes the origin, even if the
    HTML left the origin off of the href it specified.
    For example, we can write simply <a href="/">...</a>
    and then when we query the <a> for its `href`, it can
    return "http://votingtheory.org:3001/". So, before
    going further, we have to strip off the origin if it
    is present. The bulk of the present procedure works
    with the path starting with "/", possibly followed by
    a search query. For example, "/" by itself is valid
    and refers to the home page.
 */
  if (concrete.match("//")) {
    try {
    t.auri = new URL(concrete);
    } catch (err) {
      return t[404](`Saw "//" in URI, but can't parse it as a URL. ${err}`)
    };
    t.href = t.auri.pathname + t.auri.search;
    t.auri = undefined;
  } else t.href = concrete;
  t[404] = reason => ({ [0]: 404, reason:
    `${JSON.stringify(t.href)}: ${reason}`
  });

  /*
    Here we have been classifying the pathname and saying that for some
    pathnames, we would require a search query, and for the rest, we would
    forbid a search query. This does not exactly work out for
    "/archive/search", because it should take an optional search query.
  */

  if ('/'        === t.href) return {[0]: 'home',    title: "Voting Theory"};
  if ('/archive' === t.href) return {[0]: 'archive', title: "Archive"};
  if ("/archive/search" === t.href) return {
    [0]: 'search', title: "Archive -- search"
  };
  t.parts = t.href.split('?');
  if (2 !== t.parts.length) return t[404](
    "Exhausted query-free path possibilities but did not see query.",
  );
  try {
    t.aq = new URLSearchParams(t.parts[1]);
  } catch (err) {
    return t[404]("Ill-formed search string after question mark.")
  };
  if ("/archive/topics" === t.parts[0]) {
    if (1 !== Array(t.aq.keys()).length) return t[404](
      "Recognized \"/archive/topics?\" but" +
      " expected exactly one entry in search query."
    );
    /* Recognize same paths as old code did, so as not to confuse robots. */
    t.where_str = t.aq.get('where');
    if (null === t.where_str) return t[404](
      "Recognized \"/archive/topics?\" but expected a 'where' argument."
    );
    try {
      t.where = JSON.parse(t.where_str)
    } catch (err) { return t[404](
      "'where' argument does not parse as JSON."
    )};
    if (
      1 === Object.entries(t.where).length &&
      (t.catid = t.where['orig.category_id'])
    ) return {
      [0]: 'topics', category_id: t.catid,
      title: `Archive topics in category ${t.catid}`
    };
    return {
      [0]: 'topics', where: t.where,
      title: `Archive topics ${JSON.stringify(t.where)}`
    }
  }; // topics
  if ("/archive/posts" === t.parts[0]) {
    if (1 !== Array(t.aq.keys()).length) return t[404](
      "Recognized \"/archive/posts?\" but" +
      " expected exactly one entry in search query."
    );
    t.where_str = t.aq.get('where');
    if (null === t.where_str) return t[404](
      "Recognized \"/archive/posts?\" but expected a 'where' argument."
    );
    try {
      t.where = JSON.parse(t.where_str)
    } catch (err) { return t[404](
      "'where' argument does not parse as JSON."
    )};
    if (
      1 === Object.entries(t.where).length &&
      (t.topic_id = t.where['topic_id'])
    ) return {
      [0]: 'posts', topic_id: t.topic_id,
      title: `Archive posts in topic ${t.topic_id}`
    };
    if (
      1 === Object.entries(t.where).length && (t.id = t.where['_id'])
    ) return {
      [0]: 'post', id: t.id,
      title: `Archive post ${t.id}`
    };
    return {
      [0]: 'posts', where: t.where,
      title: `Archive posts ${JSON.stringify(t.where)}`
    }
  }; // posts
  if ("/archive/search" === t.parts[0]) {
    t.draft = {[0]: 'search', title: "Archive -- search"};
    t.draft.where = s.structureFromAquery(t.aq);
    return t.draft
  }; // search
  return t[404]("Unrecognized path.")
};

s.concrete = abstract => {
  let t = {abstract};
  switch (t.abstract[0]) {
  case 'home':    return "/home";
  case 'archive': return "/archive";
  case 'topics':
    t.where = t.abstract.where ||
      {'orig.category_id': t.abstract.category_id};
    t.aq = new URLSearchParams({where: JSON.stringify(t.where)});
    return "/archive/topics?" + t.aq.toString();
  case 'posts': /* plural */
    t.where = t.abstract.where || {topic_id: t.abstract.topic_id};
    t.aq = new URLSearchParams({where: JSON.stringify(t.where)});
    return "/archive/posts?" + t.aq.toString();
  case 'post':  /* Just one by itself */
    t.where = {_id: t.abstract.id};
    t.aq = new URLSearchParams({where: JSON.stringify(t.where)});
    return "/archive/posts?" + t.aq.toString();
  case 'search':
    const path = "/archive/search";
    if (! t.abstract.where) return path;
    return `${path}?${s.aqueryFromStructure(t.abstract.where).toString()}`
  default: return "/404"
  } // switch
};

s.aqueryFromStructure = given => new URLSearchParams(
  Object.entries(given).map(([k, v]) => [k, JSON.stringify(v)])
);
s.structureFromAquery = q => {
  let r = {};
  for (let [k, v] of q.entries()) {
    try {r[k] = JSON.parse(v)}
    catch {}
  };
  return r
};

s.aqueryFromArray = them => {
  `Encode the given array as an abstract query that
  when made concrete, can be included as the search query string as part
  of a URI.`;
  let it = new URLSearchParams(them.map(v => JSON.stringify(v)).entries());
  it.append('isArray', 1);
  return it
};

s.arrayFromAquery = aq => {
  `Assuming the given abstract query is an encoding of an array, return the
  array. Not taking on much of a requirement here to check whether there
  are extraneous elements in the input, as though it had not really been
  encoded from an array. But we don't want to throw in any event. In the
  worst case, return an empty array.`;
  return Array.from( ( function* () {
    let x = 0;
    while (aq.has(x)) {
      try {
        yield JSON.parse(aq.get(x++))
      } catch {return}
    }
  })())
};
