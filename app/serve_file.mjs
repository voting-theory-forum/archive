let fs;

const reject = ({res, msg}) => {
  res.setHeader("Content-Type", "text/plain; charset=utf-8");
  res.writeHead(404);
  res.end(msg || 'NOT FOUND\n');
}

const gotReq = async function (spec) {
  const {
    req, res, reqdescr, req_a_url, pathname, type
  } = spec;
  if (pathname.match(/\.\./)) return reject({res, msg: "Nice try."});
  const file_path = "fixed_assets" + pathname;
  try {
    const content = await fs.readFile(file_path);
    const cc = spec.cacheControlOverride || "no-store,max-age=0";
    res.setHeader("Cache-Control", cc);
    res.setHeader("Content-Type", type || "text/javascript; charset=utf-8")
      ;
    res.writeHead(200); // OK
    res.end(content);
  } catch(err) {
    console.error(err);
    reject(spec)
  }
};

export async function asInstalled (env) {
  fs = await import('fs/promises');
  return {gotReq}
}
