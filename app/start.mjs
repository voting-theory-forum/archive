/*
  This is the "start" in the "app" subdir for the server side.
  The top-level "start" invokes this via the version of the module manager
  for the server side.
*/

let websrv, handle;

const init = function () {
  websrv.init(handle)
}

export async function asInstalled (env) {
  const {mgr} = env;
  app.mgr = lib.mgr = mgr;
  app.server_version = 17; // rid of original heartbeep code from front end.
  const websrvp = mgr.find("lib/websrv.mjs");
  const dbp     = mgr.find("app/db.mjs");
  const handlep = mgr.find("app/got_web_request.mjs");
  await Promise.all([websrvp, dbp, handlep]).catch(console.error);
  websrv = await websrvp;
  handle = await handlep;
  return {init}
}
