/*
  Complete to the degree required for the purposes of the back
  end, the routing of the request that came over the Web, given that other code
  has already determined that the appropriate response will be in HTML.
*/

/*
  In the implementation below, we always send the home page regardless of the
  URI. The front end has the responsibility to substitute the appropriate view.
*/

let s = {};

export async function asInstalled ({mgr}) {
  s.basicPageIndex = await mgr.find("app/basic_page_index.mjs");
  return {go: s.go, _: s}
}
s.debug = () => {globalThis.s = s};

/* link rel="icon" href="/img/logo-of-pallor.png" */
s.generic = () => `<!DOCTYPE html>
<html>
  <head>
    <meta charset="UTF-8">
    <meta name="viewport" 
      content="width=device-width, initial-scale=1"
    >
    <title>Voting-Theory Forum</title>
    <link href="/style.css" rel="stylesheet">
    <script src="/lib/commonmark.js"></script>
  </head>
  <body>
    <div id=header class=header>
      <div class="banner-wrapper of_darkness">
        <a id=banner href="/" class="banner of_darkness">
          <img class=logo src="/img/logo.png"
          /><div class="stack">
            <div class=forum>Voting-Theory Forum</div>
            <div class=science>
              the science of collective decisions
            </div>
          </div>
        </a>
      </div>
      <div id=std-buttons class=std-buttons>
        <a class=button id=discussion-button
          href="https://www.votingtheory.org/forum"
        >Discussion</a>
        <a class=button id=archive-button
          href="/archive"
        >Archive</a>
        <a class=button id=about-button
          href="https://www.votingtheory.org/` +
            `forum/category/37/forum-policy"
        >About</a>
      </div>
    </div>
    <div id=main-body class=main-body>
      <p>
        Welcome. This discussion forum was created as a 
        space for those who are passionate about voting 
        theory, electoral policy, and election reform to 
        come together in the hope that when we work 
        together, we can collectively contribute 
        something of value to the movement.
      </p>
      <p>
        In building this, the next generation in a series of
        election-science forums that have come before,
        we strive to cultivate a space where ideas can 
        cross-pollinate, and where activists, experts, and 
        everyday people can connect, share, and learn.
      </p>
    </div>
    <div id=footer class=footer>
    </div>
    <script type=module>
      let app = {}, lib = {};
      globalThis.app = app;
      globalThis.lib = lib;
      app.server_version = ${app.server_version};
      import * as mgr_code from "/module_manager.mjs";
      let mgr = app.mgr = lib.mgr = mgr_code.asInstalled();
      mgr.find("app/start.mjs").then(a => a.init()).catch(console.error);
    </script>
  </body>
</html>
`;

s.go = function ({req, res}) {
  res.setHeader("Content-Type", "text/html; charset=utf-8");
  res.writeHead(404 == s.basicPageIndex.abstract(req.url)[0] ? 404 : 200);
  res.end(s.generic());
}
