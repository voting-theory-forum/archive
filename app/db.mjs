// db -- database

const MongoClient = lib.require("mongodb").MongoClient;

export async function asInstalled (spec) {
  let {mgr} = spec;
  let {db_pw} = await mgr.find("config/secret.mjs").
    catch(console.error);
  // Password has none of the special characters that would
  // require URI encoding with "%".
  // https://docs.mongodb.com/manual/reference/connection-string/
  let db_conn_uri = ( () => {
    const scheme = "mongodb";
    const host = "127.0.0.1";
    const port = 27017;
    const defaultauthdb = "archive";
    const luser = "archive";
    const pw = db_pw;
    return scheme +
      "://" +
      luser +
      ":"   +
      db_pw +
      "@"   +
      host  +
      ":"   +
      port  +
      "/"   +
      defaultauthdb
  })();
  // The docs say you can send some of the above info with
  // separate args instead of stuffing it into the URI,
  // but testing showed that doing that doesn't work.
  let mongo_client = new MongoClient(
    db_conn_uri, {useUnifiedTopology: true}
  );
  app.mongo_client = mongo_client;
  await mongo_client.connect().catch(console.error);
  const db = app.db = mongo_client.db("archive");
  for (let n of ['categories', 'topics', 'posts', 'users'])
    app[n] = db.collection(n);
  console.log("Database open.");
  return app
}
