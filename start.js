'use strict';
/*
  START HERE (top level)
*/
globalThis.app || (globalThis.app = {});
globalThis.lib || (globalThis.lib = {});
lib.require = require;
import("./module_manager.mjs").then( exports => {
  let mgr = exports.init();
  mgr.find("app/start.mjs").then(a => a.init()).catch(console.error);
}).catch(console.error)

