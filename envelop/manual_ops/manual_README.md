# manual_ops -- manual operations #

Material in this subdirectory is designed to aid the stuffing of commands 
into a REPL to do operations
that might have to be done by an administrator or an enveloper under 
circumstances that could be described. An example would be receiving an update
of the raw data for the archive.

In order to work, some of the code in this directory
might have to be adapted to changes that have 
happened to the infrastructure parts in the main code.

```
cd $repo
node --trace-warnings
require("./manual_ops");
app.mgr.find("app/db.mjs").then(() => console.log("Got it."));
Object.keys(app);
Object.keys(app.db); /* That's Mongo stuff, not mine. */

t = {}; /* tmp */
t.rec = {_id: 81, name: 'RobBrown'};
t.p = app.users.findOneAndReplace(
  {_id: t.rec._id}, // Filter.
  t.rec,            // Replacement values or new record.
  {upsert: true, checkKeys: true}
);
t.p.then( (res, rej) => {
  t.res = res;
  t.rej = rej
});
t.rej;
t.res

t.mo = [
{_id: 116, name: 'cfrank'},
{_id: 17, name: 'parker_friedland'},
{_id: 51, name: 'Keith_Edmonds'},
{_id: 49, name: 'BTernaryTau'},
{_id: 61, name: 'Toby_Pereira'},
{_id: 87, name: 'Essenzia'},
{_id: 12, name: 'NoIRV'},
{_id: 35, name: 'waugh'},
{_id: 30, name: 'Marylander'},
{_id: 27, name: 'Nealmcb'},
{_id: 19, name: 'Sara_Wolf'},
{_id: 28, name: 'rkjoyce'},
{_id: 47, name: 'AssetVotingAdvocacy'},
{_id: 48, name: 'Abd'},
{_id: 3, name: 'Jameson-Quinn'},
{_id: 56, name: 'wbport1'},
{_id: 24, name: 'Ciaran'},
{_id: 7, name: 'psephomancy'},
];

t.a = async function () {
  for (const who of t.mo) {
    const r = await app.users.findOneAndReplace(
      {_id: who._id}, // Filter.
      who,            // Replacement values or new record.
      {upsert: true, checkKeys: true}
    ).catch(console.error);
    console.log(r)
  }
};
t.a()

```
