'use strict';

// import -- import the old posts and responses from the
// export files from the old discussion forum.

/*
Keys in an export file [ 'categories', 'groups', 'topics' ].
Topic keys [
  'id',          'title',
  'created_at',  'views',
  'category_id', 'closed',
  'archived',    'archetype',
  'posts'
].
Keys in a post [
  'id',
  'user_id',
  'post_number',
  'raw',
  'created_at',
  'reply_to_post_number',
  'hidden',
  'hidden_reason_id',
  'wiki'
].
*/

let app = org.votingtheory;
const fs = require("fs").promises;
require("../misc/db");
const what = require("../misc/what");

let Beh = {};
app.import = function (filename) {
  let state = Object.create(Beh);
  app.debug = state;
  state.filename = filename;
  state.collections_open = what();

  state.blob = fs.readFile(filename, "utf8");

  app.db.then( db => {
    state.db = db;
    for (let n of ['categories', 'topics', 'posts']) {
      let c = db.collection(n);
      if (c.then) throw "Eh?";
      state[n] = c
    };
    state.collections_open.resolve(true);
    return true
  });

  return state.blob.then(have_blob.bind(state)).
    catch(app.report_error)
};
const have_blob = function (rblob) {
  this.blob = rblob;
  this.chunk = JSON.parse(rblob);
  console.log(app.keys(this.chunk));
  delete this.blob;
  return this.collections_open.asker.then(
    have_chunk_and_collections_open.bind(this)
  ).catch(app.report_error)
};
const have_chunk_and_collections_open = function () {
  this.init();
  delete this.collections_open;
  for (let c of this.chunk.categories) {
    const our_rep = { // Our internal representation.
      orig: c,
      _id: c.id
    };
    this.update(this.categories, our_rep)
  };
  this.orphan_post_ct = 0;
  typeof {} === typeof app.topic_ids_seen ||
    (app.topic_ids_seen = {});
  typeof {} === typeof app.post_ids_seen ||
    (app.post_ids_seen = {});
  for (let t of this.chunk.topics) {
    switch (app.topic_ids_seen[t.id]) {
    case undefined: app.topic_ids_seen[t.id] = 1; break;
    default: throw "Duplicate topic ID!"
    };
    let limited_t = {};
    for (let f in t) if (f != 'posts') limited_t[f] = t[f];
    let posts_by_number = {};
    this.current_topic = t; // for debugging
    this.topic_posts_by_number = posts_by_number;
    let roots = [];
    for (let p of t.posts) {
      switch (app.post_ids_seen[p.id]) {
      case undefined: app.post_ids_seen[p.id] = 1; break;
      default: throw "Duplicate post ID!"
      };
      let record = {_id: p.id, orig: p, topic_id: t.id};
      let shell = {record, children: []};
      posts_by_number[p.post_number] = shell
    };
    for (let n in posts_by_number) {
      let shell = posts_by_number[n];
      let {record} = shell;
      let {orig} = record;
      let {reply_to_post_number} = orig;
      if (null === reply_to_post_number)
        roots.push(shell);
      else {
        let parent_shell =
          posts_by_number[reply_to_post_number];
        if (parent_shell) {
          parent_shell.children.push(shell);
          record.reply_to_post_id = parent_shell.record._id
        } else {
          this.orphan_post_ct++;
          record.is_orphan = true;
          roots.push(shell)
        };
      };
    };
    // Going to sort in place, because JS supports that and
    // not any other sort, and because it won't hurt.
    const deep_sort = an_array => {
      an_array.sort( (a, b) => {
        const ad = a.record.orig.created_at;
        const bd = b.record.orig.created_at;
        return ad < bd ? -1 : ad === bd ? 0 : 1
      });
      for (let e of an_array) deep_sort(e.children);
      return true
    };
    deep_sort(roots);
    let order = 0;
    const traverse = spec => {
      const {these, depth} = spec
      for (let e of these) {
        e.record.depth_first_traversal_order = order++;
        e.record.depth = depth;
        traverse({these: e.children, depth: depth + 1})
      }
    };
    traverse({these: roots, depth: 0});
    let ct = 0;
    for (let n in posts_by_number) {
      let shell = posts_by_number[n];
      let {record} = shell;
      record.children_count = shell.children.length;
      this.update(this.posts, record);
      ct++
    };
    const topic_rec = {
      _id: limited_t.id,
      orig: limited_t,
      posts_trees_count: roots.length,
      deep_posts_count: ct
    };
    this.update(this.topics, topic_rec)
  };
  this.its_the_final_countdown = true;
  return this.mood.asker;
};
Beh.init = function () {
  this.outst_op_ct = 0;
  this.inserted_ct = 0;
  this.updated_ct  = 0;
  this.error_ct    = 0;
  this.its_the_final_countdown = false;
  this.mood = what();
  return true;
}
Beh.update = function (coll, rec) {
  const find = coll.findOneAndReplace(
    {_id: rec._id}, // Filter.
    rec,            // Replacement values or new record.
    {               // Options:
      upsert: true, // Update or insert.
      checkKeys: true
    }
  );
  this.outst_op_ct++;
  find.then( r => {
    --this.outst_op_ct;
    const leo = r.lastErrorObject;
    if (leo.n == 1 && leo.updatedExisting && r.ok)
      this.updated_ct++;
    else if (! r.ok || leo.n != 1) {
      this.error_ct++;
      this.error_example = r;
    } else {
      this.inserted_ct++;
      this.inserted_example = r
    };
    if (
      0 === this.outst_op_ct && this.its_the_final_countdown
    ) if (0 == this.error_ct) {
      console.log(this.updated_ct, "updates.");
      console.log(this.inserted_ct, "insertions.");
      console.log(this.orphan_post_ct, "orphan posts");
      delete app.debug.chunk;
      this.mood.resolve(true)
    } else {
      console.log(this.error_ct, "errors!");
      console.log("for example,", this.error_example);
      this.mood.resolve([false, this])
    }
    return this.error_ct === 0
  }).catch(app.report_error);
  return this.outst_op_ct
}

module.exports = true;
console.log("import");

