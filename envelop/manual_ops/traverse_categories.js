/*
  Manual operation to store with the categories, sequence numbers reflecting
  their depth-first traversal.
*/
( async function () {
  await app.mgr.find("app/db.mjs");
  tmp.cur = app.categories.find();
  tmp.cats = await tmp.cur.toArray();
  tmp.cur.destroy();
  delete tmp.cur;

  tmp.shells = tmp.cats.map( e => ({
    orig: e.orig,
    rec: e,
    children: []
  }));
  tmp.shells_by_id = {};
  tmp.shells.forEach( e => {
    tmp.shells_by_id[e.orig.id] = e
  });
  tmp.roots = [];
  tmp.shells.forEach( e => {
    let mommy = e.orig.parent_category_id;
    if (mommy) tmp.shells_by_id[mommy].children.push(e);
    else tmp.roots.push(e);
  });

  tmp.recurse = function (spec) {
    const {sibs, depth} = spec;
    sibs.forEach( e => {
      e.depth = depth;
      e.depth_first_traversal_order = tmp.seqno++;
      tmp.recurse({sibs: e.children, depth: depth + 1})
    })
  }

  tmp.seqno = 1;
  tmp.recurse({sibs: tmp.roots, depth: 0})

  tmp.updates = tmp.shells.map( e => ({ updateOne: {
    filter: {_id: e.orig.id},
    update: { $set: {
      depth: e.depth,
      depth_first_traversal_order:
        e.depth_first_traversal_order
    }}
  }}));

})();

tmp.huah = async function () {
  tmp.result = await app.categories.bulkWrite(tmp.updates);
}

