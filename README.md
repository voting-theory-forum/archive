# Archive #

Let's publish the old discussion posts and replies, so
people can cite them and refer back.

## Genesis ##
```
cd
mkdir -p projects
cd projects
git clone git@bitbucket.org:voting-theory-forum/archive.git
cd archive
git config user.email m2hh2kmhsn@snkmail.com
git config user.name "Jack WAUGH"
npm init
npm install mongodb
npm install commonmark
npm remove express
```
## Reference ##

Nearest thing to a reference for the input language of
the old forum:
https://meta.discourse.org/t/post-format-reference-documentation/19197/2

## Misc Snippets ##
```
cd $HOME/projects/archive

ps -U $LOGNAME -o pid,args
ps -U $LOGNAME -o pid,args | grep node

setsid node index.js >log.log 2>&1&


set -u
repo=`pwd`

node

app.import(
  "../../import/category-export-2020-08-18-003710.json"
);
app.import(
  "../../import/category-export-2020-08-18-003349.json"
);
app.import(
  "../../import/category-export-2020-08-18-003745.json"
);
app.import(
  "../../import/category-export-2020-08-18-003822.json"
);

```
