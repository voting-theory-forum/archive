// Make secret.js look kind of like what follows, but
// substitute the password from your offline vault of
// secrets.

// secret.js is named in .gitignore, so won't be saved in
// the published repo.

'use strict';
module.exports = {

  // The database password has none of the special 
  // characters that would require URI encoding with "%".
  // https://docs.mongodb.com/manual/reference/connection-string/

  // This is the user password for user "archive" of
  // database "archive", NOT the admin password for the
  // DBMS server instance.

  db_pw: 'whatever it is'
}
