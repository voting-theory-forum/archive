// Make overt.js look kind of like what follows, but
// substitute the appropriate values, e. g. for
// envelopment vs. production.
// overt.js is named in .gitignore, so won't be saved in
// the repo upstream.
'use strict';
module.exports = {
  host: 'votingtheory.org',
  port: 3000
}
