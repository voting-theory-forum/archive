// Make overt.mjs look kind of like what follows, but
// substitute the appropriate values.
// overt.mjs is named in .gitignore, so won't be saved in
// the repo upstream.

const host = 'votingtheory.org';
const port = 3001;
export {host, port}

