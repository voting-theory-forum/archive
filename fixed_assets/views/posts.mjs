let s = {};

export const asInstalled = async function ({mgr}) {
  await mgr.find("lib/pgm.mjs"         ).catch(console.error);
  await mgr.find("lib/browser_utls.mjs").catch(console.error);
  await mgr.find("app/uhura.mjs"       ).catch(console.error);
  await mgr.find("app/helpers.mjs"     ).catch(console.error);
  await mgr.find("app/cache.mjs"       ).catch(console.error);
  let pub = {};
  for (let k in s) pub[k] = s[k].bind(pub);
  return app.posts_view = pub;
};

s.go = function (args) {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  this.args = args;
  app.stdQuash();
  this.running = true;
  app.mainBody.appendChild(this.heading = div());
  app.mainBody.appendChild(this.body    = div());
  app.posts.find(
    {topic_id: this.args.topic_id}, {sort: ['depth_first_traversal_order']}
  ).dereference(console.error)(this.populate_body)
  lib.play(this.populate_heading_and_title);
};

s.populate_heading_and_title = function* () {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  let t = {};
  this.dbg_heading_and_title = t;
  t.topic_sgl = yield app.topics.find({_id: this.args.topic_id}).
    dereference(console.error);
  t.topic_rec = t.topic_sgl.car;
  t.topic_sgl.cdr.cancel();
  t.cat_rec = yield app.stdRunner.call3(
    console.error
  )(app.cache.categoryFromId(t.topic_rec.orig.category_id));
  t.topic_name = t.topic_rec.orig.title;
  t.catname = t.cat_rec.orig.name;

  document.title = `Archive -- ${t.catname} -- ${t.topic_name}`;
  this.heading.appendChild(app.renderedArchiveHeader);
  this.heading.appendChild(app.renderHeaderFromCategory(t.cat_rec));
  this.heading.appendChild(app.renderHeaderFromTopic(t.topic_rec));
};

s.populate_body = function (recs) { if (recs.isCons) {
  if (this.running) {
    const {
      div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, 
      h6, li, ol, ul
    } = lib.tags;
    let t = {post: recs.car};
    t.post_head = div({classes: ['post-head']});
    {
      const post = t.post;
      const area = t.post_head;
      app.stdRunner.call3(
        console.error
      )(
        app.renderHeaderComponentsFromPost(t.post)
      )( components => { for (const each of components)
        area.appendChild(each)
      })
    };
    this.body.appendChild(t.post_head);
    t.post_body = app.renderBodyFromPost(t.post)
    this.body.appendChild(t.post_body);

    recs.cdr.dereference(console.error)(this.populate_body) // recurse
  } else recs.cdr.cancel()
}};

s.stop = function () {
  this.running = false
}

