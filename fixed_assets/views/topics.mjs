let s = {};

export const asInstalled = async function ({mgr}) {
  
  await mgr.find("lib/browser_utls.mjs").catch(console.error);
  await mgr.find("app/uhura.mjs"       ).catch(console.error);
  await mgr.find("app/helpers.mjs"     ).catch(console.error);
  await mgr.find("app/cache.mjs"       ).catch(console.error);

  let pub = {};
  let v = Object.create(s);
  v.populate_heading_and_title = s.populate_heading_and_title.bind(v);
  v.populate_body = s.populate_body.bind(v);
  pub._ = v;
  pub.go = function (arg) {this._.go(arg)};
  pub.stop = function () {this._.stop()};
  return app.topics_view = pub;
};

s.go = function (args) {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  this.running = true;
  app.stdQuash();
  this.heading = div();
  this.body = div();
  app.mainBody.appendChild(this.heading);
  app.mainBody.appendChild(this.body);
  this.args = args;
  app.stdRunner.call3(console.error)(
    app.cache.categoryFromId(args.category_id)
  )(s.populate_heading_and_title.bind(this));
  app.topics.find({'orig.category_id': args.category_id}).
    dereference(console.error)(s.populate_body.bind(this))
};

s.populate_heading_and_title = function (cate) {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  const name = cate.orig.name;
  document.title = `Archive -- ${name}`;
  this.heading.appendChild(app.renderedArchiveHeader);
  this.heading.appendChild(app.renderHeaderFromCategory(cate));
};

s.stop = function () {
  this.running = false
};

s.populate_body = function (records) { if (records.isCons) {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  if (this.running) {
    let topic_elt = div({class: 'topic'});
    const firstRecord = records.car;
    topic_elt.appendChild(app.renderHeaderFromTopic(firstRecord));
    /* for (const [k, v] of this.concatenatedEntriesOf(firstRecord))
      topic_elt.appendChild(p(
        span({class: 'label'}, k, ": "), JSON.stringify(v)
      )); */
    topic_elt.style.paddingLeft = `${firstRecord.depth}em`;
    this.body.appendChild(topic_elt);
    records.cdr.dereference(console.error)(s.populate_body.bind(this))
  } else records.cdr.cancel()
}};

s.concatenatedEntriesOf = function* (an_obj) {
  let patsy = Object.entries(an_obj);
  patsy.sort((a, b) => a[0] < b[0] ? -1 : 1);
  for (const [k, v] of patsy)
    if (null !== v && typeof {} === typeof v)
      for (const [kk, vv] of s.concatenatedEntriesOf(v))
        yield [`${k}.${kk}`, vv];
    else yield [k, v]
};

s.test = () => {
  globalThis.s = s;
  globalThis.v = app.topics._
}
