let s; // main parts of Script.
let t; // temporary.
let d; // document help.

export const asInstalled = async function ({mgr}) {
  await mgr.find("app/basic_page_index.mjs").catch(console.error);
  await mgr.find("lib/pgm.mjs"             ).catch(console.error);
  await mgr.find("lib/browser_utls.mjs"    ).catch(console.error);
  await mgr.find("app/uhura.mjs"           ).catch(console.error);
  await mgr.find("app/helpers.mjs"         ).catch(console.error);
  await mgr.find("app/cache.mjs"           ).catch(console.error);
  d = lib.tags;
  let state = {};
  t.discipline(state, s);
  app.archive = state;
  return state;
};

s = {};

t = {};

t.discipline = (tgt, org) => {
  for (const k in org) tgt[k] = org[k].bind(tgt)
};

s.stop = function () {
  this.running = false
};

s.go = function* () {
  app.stdQuash();
  this.area = document.createElement('div');
  app.mainBody.appendChild(this.area);
  this.running = true;
  document.title = "Archive";
  this.area.appendChild(app.renderedArchiveHeader);
  for (
    let rest = yield app.cache.allCategories();
    rest.isCons;
    rest = yield rest.cdr
  ) {
    const cate = rest.car;
    if (! this.running) return false;
      /* Don't cancel the rest of the list, because a background task is
        also consuming it. Maybe the problem is I don't explicitly split
        the asker.
      */
    this.area.appendChild(app.renderHeaderFromCategory(cate))
  }
}
