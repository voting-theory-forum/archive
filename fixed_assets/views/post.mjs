/* A single post by itself. */

let s = {};

export const asInstalled = async function ({mgr}) {
  await mgr.find("lib/pgm.mjs"         ).catch(console.error);
  await mgr.find("lib/browser_utls.mjs").catch(console.error);
  await mgr.find("app/uhura.mjs"       ).catch(console.error);
  await mgr.find("app/helpers.mjs"     ).catch(console.error);
  await mgr.find("app/cache.mjs"       ).catch(console.error);
  return app.post_view = Object.create(s)
}

s.stop = function () {};

s.go = function (args) {
  this.args = args;
  lib.play(this.main.bind(this));
  app.stdQuash();
};

s.main = function* () {

  let t = this;

  t.posts = yield app.posts.find(
    {_id: t.args.id}
  ).dereference(console.error);
  t.posts.cdr.cancel();
  t.post = t.posts.car;
  t.topics = yield app.topics.find(
    {_id: t.post.topic_id}
  ).dereference(console.error);
  t.topics.cdr.cancel();
  t.topic = t.topics.car;
  t.category = yield app.stdRunner.call3(
    console.error
  )(app.cache.categoryFromId(t.topic.orig.category_id));
  let hdr_compon = yield app.stdRunner.call3(
    console.error
  )(
    app.renderHeaderComponentsFromPost(t.post)
  );

  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  app.mainBody.appendChild(app.renderedArchiveHeader);
  app.mainBody.appendChild(app.renderHeaderFromCategory(t.category));
  app.mainBody.appendChild(app.renderHeaderFromTopic(t.topic));
  app.mainBody.appendChild(h4(
    {style: {textAlign: 'left', fontFamily: 'sans'}}, ...hdr_compon
  ));
  app.mainBody.appendChild(app.renderBodyFromPost(t.post))
}
