let s = {}; /* this Script */
let d;      /* Document-related help */
export const asInstalled = async function ({mgr}) {
  await mgr.find("lib/browser_utls.mjs").catch(console.error);
  await mgr.find("app/uhura.mjs"       ).catch(console.error);
  await mgr.find("lib/pgm.mjs"         ).catch(console.error);
  await mgr.find("app/helpers.mjs"     ).catch(console.error);
  await mgr.find("app/cache.mjs"       ).catch(console.error);
  d = lib.tags;
  let state = Object.create(s);
  state.boss = state;
  for (let k in s)
    if ("function" === typeof s[k]) state[k] = s[k].bind(state);
  return app.search = state
};

s.stop = function () {
  /*
    app.stdQuash expects every view module to have a "stop" entry point.
    A call to "stop" means we are being ousted from control of the document, 
    either permanently or temporarily. We should stop any background activity.
    We must avoid grabbing control of the document until invited back via a
    call on "go". If we are holding on to any significant amounts of memory,
    it's a good time to let go of it.
  */
  /*
    In this search code, we may need a local operation to stop receiving
    search results, distinct from the "stop" described above as meaning we
    have to give up control.
  */
  this.running = false;
  for (const k of [

    'category_info_by_id',     'topic_info_by_id',     'executiveButton',
    'queryArea',               'topSwitchEditor',          'controlArea',
    'outOfDateWarningArea',      'resultsArea',           'progressArea',
    'stopButton',              'goalForExecution'

  ]) this[k] = undefined;
};
s.stopExecution = function () {
  this.running = false;
  if (this.stopButton) this.stopButton.disabled = true;
};
s.indicateRunning = function () {
  this.running = true;
  this.stopButton.disabled = false;
};

s.go = function (view_spec) {
  /* Put this view into effect. */
  this.area = app.mainBody;
  app.stdQuash();
  document.title = "Archive -- Search";
  this.area.appendChild(d.h1("Archive"));
  this.area.appendChild(d.h2("Search"));
  this.area.appendChild(this.queryArea = d.div({class: 'search-query'}));
  this.topSwitchEditor = this.create(s.SwitchEditor);
  this.topSwitchEditor.area = this.queryArea;
  if (view_spec.where) this.topSwitchEditor.goal = view_spec.where;
  this.setUpExecutiveAreas();
  this.topSwitchEditor.render();
};

s.declass = function () {
  s.empty_out_class_list(arguments[0].classList)
};
s.empty_out_class_list = list => {
  while (list.length) list.remove(list[0]);
};

s.newToken = function () {
  if (! this.nextToken) this.nextToken = 0;
  return this.nextToken++
};

s.create = function (aClass) {
  /*
    Make and return a new object whose behavior is based on aClass and that
    points to me as its structuralParent.
  */
  if (aClass.wasCreated) throw "Unsupported confusion btw abstract/concrete?";
  let it = aClass.new();
  it.wasCreated = true;
  it.boss = this.boss;
  it.structuralParent = this;
  it.token = this.boss.newToken();
  for (let n of ['check', 'render']) it[n] = aClass[n].bind(it);
  return it
};

/*
  Editors.
*/

s.SearchExpressionEditor = {};
s.SearchExpressionEditor.create = s.create;
s.SearchExpressionEditor.new = function () {
  let it = Object.create(this);
  it.behavioralParent = this;
  return it
};
s.SearchExpressionEditor.check = function () {
  const old_status = this.status;
  this.recalc();
  /* if (lib.deepEqual(old_status, this.status)) return; */
  if (this.showStatus) this.showStatus();
  setTimeout(this.structuralParent.check)
};
Object.defineProperty( s.SearchExpressionEditor, 'status', {
  get: function () {
    return {isReady: this.isReady, error: this.error, expr: this.expr}
  }, configurable: true, enumerable: true
});

s.SwitchEditor   = s.SearchExpressionEditor.new();
s.StringEditor   = s.SearchExpressionEditor.new();
s.CompoundEditor = s.SearchExpressionEditor.new();
s.DummyEditor    = s.SearchExpressionEditor.new();

s.PCREEditor    = s.StringEditor.new();
s.PhraseEditor    = s.StringEditor.new();
s.AllTheWordsEditor = s.StringEditor.new();
s.AnyOfTheWordsEditor = s.StringEditor.new();

s.ConjunctiveEditor = s.CompoundEditor.new();
s.AlternativeEditor = s.CompoundEditor.new();
s.NegativeEditor    = s.CompoundEditor.new();

s.PCREEditor         .description = "Perl-compatible regular expression";
s.ConjunctiveEditor  .description = "conjunction";
s.AlternativeEditor  .description = "alternation";
s.NegativeEditor     .description = "not";
s.PhraseEditor       .description = "exact phrase";
s.AllTheWordsEditor  .description = "all the words";
s.AnyOfTheWordsEditor.description = "any of the words";
s.DummyEditor      .description = "Please choose type."; /* Dead code. */

s.PCREEditor         .key = "PCRE";
s.ConjunctiveEditor  .key = "and";
s.AlternativeEditor  .key = "or";
s.NegativeEditor     .key = "not";
s.PhraseEditor       .key = "phrase";
s.AllTheWordsEditor  .key = "all_the_words";
s.AnyOfTheWordsEditor.key = "any_of_the_words";
s.SwitchEditor       .key = "switch";
s.DummyEditor        .key = "_choose_";
s                    .key = "search";

s.DummyEditor.render = () => {};

s.SwitchEditor.options = [
  s.PhraseEditor, s.AllTheWordsEditor, s.AnyOfTheWordsEditor,
  s.PCREEditor,   s.ConjunctiveEditor, s.AlternativeEditor, s.NegativeEditor
];
s.SwitchEditor.optionsByKey = ( () => {
  let by_key = {};
  for (const each of s.SwitchEditor.options) by_key[each.key] = each;
  return by_key
})();
s.SwitchEditor.new = function () {
  let it = this.behavioralParent.new.apply(this);
  it.read = this.read.bind(it);
  return it
};
s.SwitchEditor.render = function () {
  s.declass(this.area);
  this.typeArea = lib.tag('select')({class: 'search-type'});
  this.specArea = document.createElement('div');
  this.statusArea = d.div({class: 'status'});
  this.area.appendChild(this.typeArea);
  this.area.appendChild(this.specArea);
  this.area.appendChild(this.statusArea);
  this.optionsRenderingInfoByIndex = this.options.map( inp => {
    let t = {};
    t.type = inp;
    t.option_elt  = document.createElement('option');
    t.option_elt.value = inp.key;
    t.option_elt.appendChild(document.createTextNode(inp.description));
    return t
  });
  for (let each of this.optionsRenderingInfoByIndex)
    this.typeArea.appendChild(each.option_elt);
  this.typeArea.addEventListener('change', this.read);
  if (this.goal && this.goal[0]) this.typeArea.value = this.goal[0];
  setTimeout(this.read);
};
s.SwitchEditor.read = function () { /* private */
  /* Can be called with no argument, or with an event. */
  lib.emptyOut(this.specArea);
  const v = this.typeArea.value;
  const type = this.optionsByKey[v];
  this.chosen = this.create(type);
  if (this.goal && this.goal[0] === v) this.chosen.goal = this.goal;
  this.chosen.area = this.specArea;
  this.chosen.render();
  setTimeout( () => {
    const v = this.typeArea.value;
    if (v && "_choose_" === v)
         this.typeArea.classList.remove('chosen');
    else this.typeArea.classList.   add('chosen');
    setTimeout(this.check);
  });
};
s.SwitchEditor.showStatus = function () {
  let there = this.statusArea;
  if (! this.expr) this.recalc();
  if (! this.expr) return;
  lib.emptyOut(there);
  const say = txt => there.appendChild(document.createTextNode(txt));
  if (! this.isReady) {
    there.classList.add('err');
    say(this.error);
  }
};

s.commonOptions = {};
s.commonOptions.i =
  ['i', "Case insensitivity to match upper and lower cases."]
s.PCREEditor.staticOptions = [
  s.commonOptions.i,
  ['m', "Match ^ and $ at the beginnings and ends of lines."],
  ['s', "Make \".\" match a newline character."]
];
s.StringEditor.staticOptions = [s.commonOptions.i];
s.StringEditor.render = function () {
  const {table, tr, td, label} = d;
  const input = lib.tag('input');
  if (! this.area) throw new Error();
  s.declass(this.area);
  this.area.classList.add('search-spec');
  this.inp = input();
  this.inp.style = "width: 95%; margin-left: auto; margin-right: auto";
  this.inp.addEventListener('input', this.check);
  if (this.goal && this.goal.pattern) this.inp.value = this.goal.pattern;
  this.dynamicOptions = this.staticOptions.map( e => ({
    key:   e[0],
    descr: e[1],
    ckbx: input()
  }));
  let tab = table();
  tab.style = "border-collapse: collapse; margin-top: .3em";
  for (const e of this.dynamicOptions) {
    e.ckbx.type = 'checkbox';
    e.ckbx.addEventListener('change', this.check);
    if (e.key === 'i') e.ckbx.checked = true;
    if (this.goal && undefined !== this.goal[e.key])
      e.ckbx.checked = ! ! this.goal[e.key];
    let row = tr(label(td(e.ckbx), td(`(${e.key})`), td(e.descr)));
    row.style = "border: 1px solid  #c96c1c; border-collapse: collapse";
    tab.appendChild(row);
  };
  this.area.appendChild(this.inp);
  this.area.appendChild(tab);  
  setTimeout(this.check);
  this.goal = undefined
};

s.CompoundEditor.setUpComponentEditors = function () {
  if (this.goal && this.goal.over && this.goal.over.map)
    this.componentEditors = this.goal.over.map( e => {
      let r = this.create(s.SwitchEditor);
      r.goal = e;
      return r
    });
  else this.componentEditors = 
    [this.create(s.SwitchEditor), this.create(s.SwitchEditor)]
};
s.NegativeEditor.setUpComponentEditors = function () {
  this.componentEditors = [this.componentEditor = this.create(s.SwitchEditor)]
  if (this.goal && this.goal.of) this.componentEditor.goal = this.goal.of
};
s.CompoundEditor.canAddAndDeleteBranches = true; /* in theory */
s.NegativeEditor.canAddAndDeleteBranches = false;

s.CompoundEditor.render = function () {
  if (! this.left ) this.left  =
    d.div({class: 'search-compound-left'});
  if (! this.right) this.right =
    d.div({class: 'search-compound-right'});
  lib.emptyOut(this.area);
  this.area.appendChild(this.left);
  this.area.appendChild(this.right)
  let probe = this.area.classList;
  if (! probe.contains("search-compound-grid"))
    probe.add("search-compound-grid");
  probe = undefined;
  if (! this.componentEditors) this.setUpComponentEditors();
  lib.emptyOut(this.right);
  for (const each of this.componentEditors) {
    let area = d.div();
    this.right.appendChild(area);
    each.area = area;
    each.render()
  }
  let ce = this.componentEditors;
  const l = ce.length;
  if (l >= 2) {
    const short = l - 1;
    for (let x = 0; x < short; x++) 
      ce[x].area.style = "border-bottom: none";
    for (let x = 1; x < l; x++) 
      ce[x].area.style = "border-top: none"
  }
};

/*
  recalc --- Based on the state of my interaction with the user, recalculate
  my status. This includes an indication of whether my result is ready to be
  used in a search, an explanation of why not if not, and an expression that
  could be used to create a copy of me restored to my current state of
  interaction with the user. If everything is complete and error free, this
  expression also can go into composing the executable search constraint.
*/
s.DummyEditor.recalc = () => {};
s.DummyEditor.isReady = false;
s.DummyEditor.error = "Choose type."
s.DummyEditor.expr = {[0]: s.DummyEditor.key};
s.StringEditor.recalc = function () {
  const complaint = "An empty pattern would be tautologous.";
  const raw = this.inp.value;
  this.expr = {[0]: this.key, pattern: raw};
  for (const e of this.dynamicOptions)
    this.expr[e.key] = e.ckbx.checked ? 1 : 0;
  if (! raw) {
    this.isReady = false;
    this.error = complaint;
  } else {
    this.isReady = true;
    this.error = false;
  }
};
s.SwitchEditor.recalc = function () {
  this.isReady = this.chosen.isReady;
  this.error   = this.chosen.error;
  this.expr    = this.chosen.expr
};
s.NegativeEditor.recalc = function () {
  this.expr = {[0]: "not", of: this.componentEditor.expr}
  if (this.componentEditor.isReady) {
    this.isReady = true;
    this.error = false;
  } else {
    this.isReady = false;
    this.error = "Incomplete or erroneous subexpression."
  };
};

s.CompoundEditor.recalc = function () {
  this.expr = {[0]: this.key, over: this.componentEditors.map(e => e.expr)};
  let foundError = false;
  const size = this.componentEditors.length;
  if (0 === size) {
    foundError = this.emptyComplaint;
  } else {
    let error_count = 0;
    for (let each of this.componentEditors)
      if (! each.isReady) error_count++;
    switch (error_count) {
    case 0: break;
    case 1: foundError = "Incomplete or erroneous subexpression.";
      break;
    default:
      foundError = `${error_count} incomplete or erroneous subexpressions.`
    };
  };
  this.isReady = ! foundError;
  this.error = foundError;
};

s.ConjunctiveEditor.emptyComplaint =
  "The conjunction over no conditions would be tautologous.";
s.AlternativeEditor.emptyComplaint =
  "The alternation over no alternatives would contradict itself.";

/* End of editors. */

/*
  Translate an abstract expression for a constraint to search on, to
  MongoDB's notation.
*/
s.quote_for_pcre = str => str.replace(/[\(\)\\\^\$\.\[\]\*\+\?\{]/g, "\\$&");
s.expand = expr => {
  let words, acc, is_first;
  switch (expr[0]) {
  default: throw new Error('expand', JSON.stringify(expr));
  case 'not': return {[0]: 'not', of: s.expand(expr.of)};
  case 'and':
  case 'or':
    return {[0]: expr[0], over: expr.over.map(s.expand)};
  case 'PCRE': return expr;
  case 'phrase':
  case 'all_the_words':
  case 'any_of_the_words':
    words = expr.pattern.split(' ').filter(e => e)
  };
  switch (expr[0]) {
  case 'phrase':
    acc = "";
    acc += "\\b"; /* Word boundary. */
    is_first = true;
    for (const w of words) {
      if (! is_first) acc += "\\s+"; /* At least one space */
      is_first = false;
      acc += s.quote_for_pcre(w);
    };
    acc += "\\b";
    return {[0]: 'PCRE', pattern: acc, i: expr.i};
  case 'all_the_words':
    return { [0]: 'and', over: words.map( w => ({
      pattern: `\\b${s.quote_for_pcre(w)}\\b`,
      [0]: 'PCRE',
      i: expr.i
    }))};
  case 'any_of_the_words':
    acc = "";
    is_first = true;
    for (const w of words) {
      if (! is_first) acc += "|";
      is_first = false;
      acc += `\\b${s.quote_for_pcre(w)}\\b`;
    };
    return {[0]: 'PCRE', pattern: acc, i: expr.i}
  } /* switch */
};
s.translate_regex = expr => {
  let options_string = "";
  for (const e of s.PCREEditor.staticOptions)
    if (expr[e[0]]) options_string += e[0];
  return {$regex: expr.pattern, $options: options_string}
};
s.final_translate = ({expr, field}) => {
switch (expr[0]) {
case 'PCRE':
    return {[field]: s.translate_regex(expr)};
case 'not':
  switch (expr.of[0]) {
  case 'not': return s.translate({expr: expr.of.of, field});
  case 'PCRE': return {[field]: {$not: s.translate_regex(expr.of)}};
  case 'and':
  case 'or':
    /*
      MongoDB can't handle "not" outside of "and" or "or". So de Morgan the
      negation inward.
    */
    const m = expr.of.over.map(e => ({[0]: 'not', of: e}));
    switch (expr.of[0]) {
    case 'and': return s.translate({expr: {[0]: 'or' , over: m}, field});
    case 'or':  return s.translate({expr: {[0]: 'and', over: m}, field});
    };
  default: throw new Error(`not ${expr.of[0]}`);
  };
case 'and': return {$and: expr.over.map(e => s.translate({expr: e, field}))};
case 'or':  return {$or:  expr.over.map(e => s.translate({expr: e, field}))};
default: throw new Error('WTF');
}};
s.translate = ({expr, field}) =>
  s.final_translate({field, expr: s.expand(expr)});

/*
  Execution of the search, and telling the user about the progress and
  results of the search.
*/

s.setUpExecutiveAreas = function () {
  /*
    Set up the areas of the document that we want to use in relation to the
    execution of the search.
  */
  /* IDs are only for debugging. */
  this.area.appendChild(this.controlArea = d.p({id: 'search-control'}));
  this.area.appendChild(
    this.outOfDateWarningArea = d.p({id: 'search-out-of-date'})
  );
  this.area.appendChild(this.progressArea = d.div({id: 'search-progress'}));
  this.area.appendChild(this.resultsArea = d.div({id: 'search-results'}));
};

s.check = function () {
  const expr = this.topSwitchEditor.expr;
  if (undefined === expr) {
    /* This case occurs, but I have not figured out how. */
    return
  };
  const abstr = app.currentPage = {[0]: 'search', where: expr};
  const uri = app.basicPageIndex.concrete(abstr);
  history.replaceState(abstr, JSON.stringify(abstr), uri);
  if (this.topSwitchEditor.isReady) {
    if (! this.executiveButton) this.setUpExecutiveButton();
    this.      enableExecutiveButton();
    if (this.executedExpr && ! lib.deepEqual(this.executedExpr, expr))
      this.warnOutOfDate();
    else
      this.removeOutOfDateWarning();
    this.goalForExecution = expr;
  } else if (this.executiveButton) this.disableExecutiveButton();
};
s.executiveLegend = "Go"; /* vs say "Execute", "Huah", "Search Now", . . . */
s.setUpExecutiveButton = function () {
  let it = this.executiveButton = document.createElement('button');
  it.type = 'button';
  it.appendChild(document.createTextNode(s.executiveLegend));
  it.addEventListener('click', () => lib.play(this.execute));
  this.controlArea.appendChild(it);
  it = d.span();
  it.innerHTML = " &nbsp; ";
  this.controlArea.appendChild(it);
  it = this.stopButton = document.createElement('button');
  it.disabled = ! this.running;
  it.type = 'button';
  it.appendChild(document.createTextNode("Stop"));
  it.addEventListener('click', this.stopExecution);
  this.controlArea.appendChild(it);
};
s.enableExecutiveButton = function () {
  this.executiveButton.disabled = false
};
s.disableExecutiveButton = function () {
  this.executiveButton.disabled = true
};
s.warnOutOfDate = function () {
  const msg =
    "The results below reflect an old search using a different constraint " +
    "than the one you currently have set up above. To search on the new "   +
    `constraint, press the ${s.executiveLegend} button again.`;
  let it = this.outOfDateWarningArea;
  lib.emptyOut(it);
  it.appendChild(document.createTextNode(msg));
  it.classList.add('warning');
};
s.removeOutOfDateWarning = function () {
  let it = this.outOfDateWarningArea;
  lib.emptyOut(it);
  it.classList.remove('warning');
}

s.bail = function (err) {
  let the_p = d.p();
  the_p.appendChild(d.span("Technical failure: "));
  the_p.appendChild(d.span(err.toString()));
  this.progressArea.appendChild(the_p);
};
s.indicateStopped = function () {
  this.progressArea.appendChild(d.p("Search canceled."));
};

s.execute = function* () {
  this.removeOutOfDateWarning();
  this.indicateRunning();
  lib.emptyOut(this.resultsArea, this.progressArea);
  this.category_info_by_id = {};
  this.topic_info_by_id = {};
  this.progressArea.classList.add('warning'); /* Because not complete. */

  /* Search topics. */
  let query = s.translate({
    field: 'orig.title', expr: this.goalForExecution
  });
  let options = {};
  this.progressArea.appendChild(d.p(
    `app.topics.find(${JSON.stringify(query)}, ` +
    `${JSON.stringify(options)});`
  ));
  let ct = 0;
  let ct_fld = d.span("0");
  let ct_rpt = d.p("Count of records received: ", ct_fld);
  this.progressArea.appendChild(ct_rpt);
  let rest = yield app.topics.find(query, options).dereference(this.bail);
  while (rest.isCons) {
    if (! this.running) {this.indicateStopped(); return}
    ct++;
    lib.emptyOut(ct_fld);
    ct_fld.appendChild(document.createTextNode(ct.toString()));
    yield this.register_topic(this.bail)(rest.car);
    rest = yield rest.cdr.dereference(this.bail)
  };

  /* Search posts. */
  let sort = ['topic_id', 'depth_first_traversal_order'];
  let projection = {"orig.raw": 0};
  options = {sort, projection};
  query = s.translate({field: 'orig.raw', expr: this.goalForExecution});
  this.progressArea.appendChild(d.p(
    `app.posts.find(${JSON.stringify(query)}, ${JSON.stringify(options)});`
  ));
  ct_fld = d.span("0");
  ct_rpt = d.p("Count of records received: ", ct_fld);
  this.progressArea.appendChild(ct_rpt);
  ct = 0;
  rest = yield app.posts.find(query, options).dereference(this.bail);
  while (rest.isCons) {
    if (! this.running) {this.indicateStopped(); return}
    ct++;
    lib.emptyOut(ct_fld);
    ct_fld.appendChild(document.createTextNode(ct.toString()));
    let it = s.Post.new();
    it.boss = this;
    Object.assign(it, rest.car);
    yield it.render(this.bail);
    rest = yield rest.cdr.dereference(this.bail)
  };

  this.running = false;
  this.stopButton.disabled = true;
  this.progressArea.classList.remove('warning');
  this.progressArea.appendChild(d.p("Search complete."));
  this.executedExpr = this.goalForExecution
};

s.category_info_from_id = function (bad) { return id => good => {
  let self = this;
  lib.play( function* () {
    if (! self.category_info_by_id[id]) {
      const cat_rec =
        yield app.stdRunner.call3(bad)(app.cache.categoryFromId(id));
      let it = self.category_info_by_id[id] = {};
      Object.assign(it, cat_rec);
      it.area = d.div();
      self.resultsArea.appendChild(it.area);
      it.headerArea = app.viewHelpers.renderHeaderFromCategory(cat_rec);
      it.bodyArea = d.div();
      it.area.appendChild(it.headerArea);
      it.area.appendChild(it.bodyArea);
    }
    setTimeout(() => good(self.category_info_by_id[id]))
  })
}};
s.register_topic = function (bad) { return record => good => {
  let self = this;
  lib.play( function* () {
    let cat = yield self.category_info_from_id(bad)(record.orig.category_id);
    let top = self.topic_info_by_id[record._id] = {};
    Object.assign(top, record);
    top.category_info = cat;
    top.area = d.div();
    cat.bodyArea.appendChild(top.area);
    top.headerArea = app.viewHelpers.renderHeaderFromTopic(record);
    top.bodyArea = d.div();
    top.area.appendChild(top.headerArea);
    top.area.appendChild(top.bodyArea);
    setTimeout(() => good(top))
  })
}};
s.topic_info_from_id = function (bad) { return id => good => {
  let self = this;
  lib.play( function* () {
    let hit = self.topic_info_by_id[id];
    if (hit) return setTimeout(() => good(hit));
    let them = yield app.topics.find({_id: id}).dereference(bad);
    setTimeout (() => them.cdr.cancel());
    setTimeout (() => self.register_topic(bad)(them.car)(good))
  })
}};
s.Post = {};
s.Post.new = function () {
  let it = Object.create(this);
  for (let k of ['render', 'expand', 'contract'])
    it[k] = this[k].bind(it);
  return it
};

s.Post.render = function (bad) { return good => lib.play(( function * () {
  this.topic = yield this.boss.topic_info_from_id(bad)(this.topic_id);
  this.area = d.div();
  this.topic.bodyArea.appendChild(this.area);
  this.area.appendChild(
    this.headerArea = d.div({style: {textAlign: 'left'}})
  );
  this.area.appendChild(this.bodyArea   = d.div());
  this.area.appendChild(this.footerArea = d.div());
  let it = this.expandButton = document.createElement('button');
  it.type = 'button';
  it.appendChild(document.createTextNode("expand"));
  it.addEventListener('click', this.expand);
  this.headerArea.appendChild(it);
  const space = () => {
    let it = d.span();
    it.innerHTML = " &nbsp; ";
    this.headerArea.appendChild(it);
  }; space();
  it = this.topContractButton = document.createElement('button');
  it.type = 'button';
  it.appendChild(document.createTextNode("contract"));
  it.disabled = true;
  it.addEventListener('click', this.contract);
  this.headerArea.appendChild(it);
  space();
  let compon = yield app.stdRunner.call3(
    bad
  )(app.viewHelpers.renderHeaderComponentsFromPost(this));
  for (it of compon) this.headerArea.appendChild(it);
  setTimeout(() => good(this));
}).bind(this))};

s.Post.expand = function () { lib.play(( function* () {
  this.topContractButton.disabled = false;
  this.expandButton.disabled = true;
  let them = yield app.posts.find(
    {_id: this._id}
  ).dereference(this.boss.bail);
  them.cdr.cancel();
  this.bodyArea.appendChild(app.viewHelpers.renderBodyFromPost(them.car))
}).bind(this))};

s.Post.contract = function () {
  this.topContractButton.disabled = true;
  this.expandButton.disabled = false;
  lib.emptyOut(this.bodyArea, this.footerArea);
}
