let s = {};

export const asInstalled = async function () {
  return s;
};

s.go = function () {
  document.title = "Voting-theory Forum";
  app.stdQuash();
  app.homepg_content.forEach( e => {
    app.mainBody.appendChild(e)
  })
};

s.stop = function () {};

s.name = "home";
