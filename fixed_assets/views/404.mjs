let s = {};

export const asInstalled = async function () {
  await app.mgr.find("lib/browser_utls.mjs");
  return s
};

const body_h =`
  <div class=ludicrous>404</div>
  <div class=huge>DEMOCRACY NOT FOUND</div>
`;

s.go = function (arg) {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  document.title = "404 Not Found";
  app.stdQuash();
  app.mainBody.innerHTML = body_h;
  if (arg && arg.reason) app.mainBody.appendChild(p(arg.reason));
};
s.stop = function () {};
s.name = 404

