// Lt. Uhura -- Communications Officer.

/*
  Installing Lt. Uhura results in placing 'categories', 'topics',
  'posts' and 'users' in globalThis.app as 
  proxies for collections of records in the
  database. View code can hit them with ".find" calls.
  The args to 'find' can be anything that MongoDB can take in its 'find'.
  However, the results work differently than MongoDB's cursors.
*/

let s = {}; // script.
let t = {}; // manual testing.

export async function asInstalled () {
  await app.mgr.find("lib/lvar.mjs").catch(console.error);
  lib.list = await app.mgr.find("lib/list.mjs").catch(console.error);
  await app.mgr.find("lib/pgm.mjs").catch(console.error);
  let state = {};
  for (const entry of Object.entries(s)) {
    const {key, value} = entry;
    if (typeof (() => {}) === typeof value) state[key] = value.bind(state)
  };
  app.uhura = {_: state};
  s.Collection.publish('categories', 'topics', 'posts', 'users');
    // These names are going directly into globalThis.app.
  return app
};

// We need to whip up some AJAX.

// Object.getOwnPropertyNames(XMLHttpRequest)
/* Array [
  "length", "name", "UNSENT", "OPENED", "HEADERS_RECEIVED",
  "LOADING", "DONE", "prototype" 
] */
s.ajaxStatusNames = [
  "UNSENT", "OPENED", "HEADERS_RECEIVED",
  "LOADING", "DONE"
];
s.ajaxStatusNamesByNumber = {};
s.ajaxStatusNames.forEach( e => {
  s.ajaxStatusNamesByNumber[XMLHttpRequest[e]] = e
});
/*
  s.ajaxStatusNames.forEach( e => {
    console.log(XMLHttpRequest[e],  e)
  });

  0 UNSENT 
  1 OPENED 
  2 HEADERS_RECEIVED 
  3 LOADING 
  4 DONE
*/
s.basicAjax = bad => spec => good => {
  let req = new XMLHttpRequest();
  req.onreadystatechange = () => {
    /* if (s.verbose) {
      console.log("AJAX", s.ajaxStatusNamesByNumber[req.readyState])
    } */
    if (req.readyState === XMLHttpRequest.DONE) {
      const status = req.status;
      if (status === 0 || (status >= 200 && status < 400)) {
        setTimeout(() => good(req.responseText), 0)
      } else
        setTimeout(() => bad(`HTTP ${status}!`), 0)
    } else {
      // The state currently indicated by the readyState variable is not
      // interesting to me. Bide until it changes again.
    };
  }; // onreadystatechange.
  req.open(spec.sel || 'GET', spec.uri);
  req.send()
};

s.handle_ugly = ugly => {}; // deal with later.

/*
  Wrap the basic AJAX, which deals with strings, wrap that in app-specific
  AJAX, which understands that for this app, we always
  communicate in JSON. Furthermore, app-specific AJAX strips away ancillary
  information that comes back along with all the server responses. So all
  that is returned to callers is the meat they are interested in.
*/
s.appAjax = bad => spec => good => s.basicAjax(bad)(spec)( response_string => {
  try {
    let abstr = JSON.parse(response_string);
    s.handle_ugly(abstr.ugly);
    let msg = abstr.good;
    if (abstr.hasOwnProperty('good')) setTimeout(() => good(msg), 0);
    else if (msg = abstr.bad ) setTimeout(() => bad( msg), 0);
    else bad(
      "Uhura appAjax: Expected good or bad in top-level message from server; " +
      "received " + response_string + "."
    );
  } catch (err) { bad(
    `Uhura appAjax: JSON parser unhappy with response string from server.` +
    ` ` + err
  )}
});

s.abstract = ({from, to}) => {
  /*
    `from` is a concrete presentation received over the wire.
    Interpret its meaning into abstract terms.
    Ancillary to producing the result, we allow a side effect here of deleting
    any foreign references that we consume. So, this should not be called
    twice on the same foreign reference; it will not be referentially
    transparent in
    that case. But a logical variable yielded here may be queried more than
    once and that will work, because it keeps a memo.
  */
  const {lvar, list} = lib;
  if (null == from) return void to.resolve(list.nil);
  else if (from.isReference) return void lib.play( function * () {
    const rsp = yield s.appAjax(to.reject)({sel: 'GET', uri: from.URI});
    s.appAjax(_err => {})({sel: 'DELETE', uri: from.URI})(_rsp => {});
    to.onDemand(() => s.abstract({from: rsp, to}))
  }); else if (from.isCons) return void lib.play( function * () {
    let n = list.Cons.new();
    const carchan = lvar.new();
    const cdrchan = lvar.new();
    setTimeout(() => s.abstract({from: from.car, to: carchan}), 0);
    setTimeout(() => s.abstract({from: from.cdr, to: cdrchan}), 0);
    n.cdr = cdrchan.ask;                              // lazy cdr.
    n.car = yield carchan.ask.dereference(to.reject); // eager car.
    return void to.resolve(n)
  }); else if (from.isLiteral) return void to.resolve(from.value);
  else return void to.reject(
    `Uhura abstract: don't know how to classify ` +
    JSON.stringify(from) + `.`
  )
};

s.Collection = {
  beh: {},
  publish: function () {
    for (name of arguments) {
      app[name] = Object.create(this.beh);
      app[name].name = name
    }
  }
};
s.Collection.beh.find = function (...args) {
  let tell = lib.lvar.new();
  let urlsearch = new URLSearchParams();
  urlsearch.append('find', JSON.stringify(args));
  let uri = `/${this.name}.json?${urlsearch.toString()}`;
  s.appAjax(tell.reject)({sel: 'GET', uri})( rsp => {
    s.abstract({from: rsp, to: tell})
  });
  return tell.ask
};

/*
  From here down is not necessary.
*/

s.clockPeriod = 3000; // ms
s.incedentallyAwake = function () {
  /* Call this when Uhura is awake for reasons other than timeouts. */
}
s.timedOut = function () {
  /* This will be called when the alarm goes off. */
}
s.checkClock = function () {
  let t = {};
  t.now = Date.now();
  if (! this.wasAwake) this.wasAwake = t.now;
  if (t.now >= this.wasAwake + this.clockPeriod)
    console.log("Uhura idle", new Date());
  if (! this.timer) this.setTimer();
};
s.setTimer = function () {
  this.timer = setTimeout(this.timeout, 0)
}
s.timeout = function () {
  this.timer = false;
  this.checkClock()
}


// Support for manual testing and debugging:

s.test = () => {
  globalThis.s = s;
  globalThis.t = t
};
s.verbose = true;
t.good = r => {
  console.log("result", r);
  t.result = r;
  t.summary = "good";
}
t.bad = r => {
  console.log("baaaaad!", r);
  t.result = undefined;
  t.summary = "bad"
};
t.ajax = s.basicAjax(t.bad);
// t.ajax({path: "/categories.json"})(t.good)
t.ex0 = new URLSearchParams();
t.ex0.append('find', JSON.stringify([]))
t.ex0.toString()
// t.ajax({path: `/categories.json?${t.ex0.toString()}`})(t.good)

