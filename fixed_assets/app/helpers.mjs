/*
  These routines help the page views by rendering certain fragments of
  information in consistent ways across different views (or maybe even in
  more than one place in a given view).
*/

let s = {};

export const asInstalled = async function ({mgr}) {
  await mgr.find("lib/browser_utls.mjs").catch(console.error);
  let state = {};
  Object.assign(state, s);
  state.dateFmt = new Intl.DateTimeFormat([], {
    dateStyle: "full", timeStyle: "full",
    timeZone: "UTC",
  });
  Object.assign(app, state);
  return app.viewHelpers = state
};

s.fmtDateFld = function (it) {
  /* Given a date the way it is stored in the database, format it for 
    a person to read. Example input: 2019-06-12T14:21:33.460Z */
  const year   = + it.slice(0, 4);
  const month  = + it.slice(5, 7);
  const day    = + it.slice(8, 10);
  const hour   = + it.slice(11, 13);
  const minute = + it.slice(14, 16);
  const second = + it.slice(17, 19); /* don't care about milliseconds */
  const from_epoch = Date.UTC(year, month, day, hour, minute, second);
  const as_object = new Date(from_epoch);
  return this.dateFmt.format(as_object)
};

{
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  s.renderedArchiveHeader = ( () => {
    let gilthoniel = a(
      {href: app.basicPageIndex.concrete({[0]: 'search'})},
      "[Search Archive]"
    );
    app.linkInternally(gilthoniel);
    return div({class: 'archive-header'}, h1("Archive"), gilthoniel, div())
  })();
  s.renderHeaderFromCategory = cate => {
    let the_anchor = a(cate.orig.name);
    the_anchor.href = app.basicPageIndex.concrete({
      [0]: 'topics', category_id: cate._id
    });
    app.linkInternally(the_anchor);
    return h2(the_anchor)
  };
  s.renderHeaderFromTopic = the_topic => {
    let the_anchor = a(the_topic.orig.title);
    the_anchor.href = app.basicPageIndex.concrete({
      [0]: 'posts', topic_id: the_topic._id
    });
    app.linkInternally(the_anchor);
    return h3(the_anchor)
  };
};

s.renderHeaderComponentsFromPost = function* (the_post) {
  /*
    Return an ordered collection of rendered components for a header for a
    post, but without an opinion about what kind of element should hold them.
  */
  let acc = [];
  if (the_post.depth) {
    let sacc = ""
    for (let ctr = the_post.depth; ctr; --ctr) sacc += ">";
    acc.push(document.createTextNode(sacc + " "))
  };
  acc.push(s.renderLinkToPostGnostic({
    id:          the_post._id,
    topic_id:    the_post.topic_id,
    post_number: the_post.orig.post_number
  }));
  let name = yield app.cache.userNameForId(the_post.orig.user_id);
  acc.push( document.createTextNode(
    " " +
    "by " + name  + " " +
    "on " + this.fmtDateFld(the_post.orig.created_at) + " "
  ));
  if (the_post.reply_to_post_id) {
    acc.push(document.createTextNode("in reply to "));
    acc.push( s.renderLinkToPostGnostic({
      id:          the_post.reply_to_post_id,
      topic_id:    the_post.topic_id,
      post_number: the_post.orig.reply_to_post_number
    }));
  } else if (the_post.is_orphan) acc.push(document.createTextNode("orphan"));
  return acc
};

s.renderLinkToPostGnostic = ({id, topic_id, post_number}) => {
  /*
    Render a link to a post about which we know these
    two ways to identify it: its normal ID number, and
    secondly, the combination of the topic it is part of
    with the post's number within the topic.
    At the time of this writing, I have not established how to hit the
    database based on that second identifier. But it is what to display,
    for correspondence with the addressing in the [quote ...]...[/quote]
    construct in post bodies.
  */
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  let elt = a(`${topic_id}:${post_number}`);
  elt.href = app.basicPageIndex.concrete({[0]: 'post', id});
  app.linkInternally(elt);
  return elt
};

s.renderBodyFromPost = the_post => {
  const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul
  } = lib.tags;
  const {Parser, HtmlRenderer} = commonmark;
  let it = div({classes: ['readable-text', 'post-body']});
  let parser   = new Parser();
  let renderer = new HtmlRenderer();
  let markup = parser.parse(the_post.orig.raw);
  it.innerHTML = renderer.render(markup);
  return it
}
