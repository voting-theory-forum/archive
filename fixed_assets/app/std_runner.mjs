export const asInstalled = async function ({mgr}) {
  await mgr.find("lib/runner.mjs").catch(console.error);
  app.stdTramp = {
    defer: function (f, ...args) {
      setTimeout(() => f(...args))
    }
  };
  app.stdRunner = lib.Runner.clone();
  app.stdRunner.trampoline = app.stdTramp;
  return app
};

