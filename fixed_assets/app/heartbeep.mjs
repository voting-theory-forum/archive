// browser
let beh;
export async function asInstalled (env) {
  const {mgr} = env;
  let self = Object.create(beh);
  self.onChange = beh.onChange.bind(self);
  self.start    = beh.start   .bind(self);
  self.period = 0; // 1000 * 600; // 10 min.
  if (typeof globalThis.app != typeof {})
    globalThis.app = {};
  globalThis.app.heartbeep = self;
  setTimeout(self.start, self.period);
  return self
};
const onChange = function () {
  let xhr = this.req;
  if(xhr.readyState === XMLHttpRequest.DONE) {
    var status = xhr.status;
    console.log("heartbeep XHR status", status);
    if (status === 0 || (status >= 200 && status < 400)) {
      // The request has been completed successfully
      console.log(xhr.responseText);
      let meaning;
      try {
        meaning = JSON.parse(xhr.responseText);
      } catch {
        return setTimeout(this.start, this.period)
      };
      if ( meaning.serverVersion !=
        globalThis.app.server_version
      ) location.reload();
      setTimeout(this.start, meaning.recommendedPeriod);
    } else {
      console.error("Heartbeep request crapped out.")
    }
  }
};
const start = function () {
  this.req = new XMLHttpRequest();
  this.req.onreadystatechange = this.onChange;
  this.req.open('GET', "/lub");
  this.req.send();
};
beh = {start, onChange};
