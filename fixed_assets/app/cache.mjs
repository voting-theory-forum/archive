let s, /* script */
  t; /* testing or temporary */

export const asInstalled = async function({mgr}) {
  await mgr.find("lib/lvar.mjs" ).catch(console.error);
  await mgr.find("app/std_runner.mjs"  ).catch(console.error);
  await mgr.find("app/uhura.mjs").catch(console.error);
  lib.list = await mgr.find("lib/list.mjs").catch(console.error);
  let state = Object.create(s);
  for (const k in s) {
    const v = s[k];
    if ('function' === typeof v) state[k] = v.bind(state);
  }
  state.known_categories_by_id = {};
  state.user_info_by_id = {};
  app.cache = state;

  t.tst0();

  return state
};

s = {};
let _ = s._ = {}; /* private */

_.cate_proj = { /* Projection spec for fetching categories. */
  'depth':                       1,
  'depth_first_traversal_order': 1,
  'orig.created_at':             1,
  'orig.description':            1,
  'orig.name':                   1,
  'orig.parent_category_id':     1,
  'orig.slug':                   1
};

s.fetchUserNameForId = function* (id) {
  const they = yield app.users.find({_id: id});
  if (they.isCons) {
    they.cdr.cancel();
    return they.car.name
  } else {
    return id.toString()
  }
};
s.userNameForId = function* (id) {
  let hit;
  while (! (hit = this.user_info_by_id[id]))
    this.user_info_by_id[id] = yield this.fetchUserNameForId(id);
  return hit
}

s.categoryFromId = function* (id) {
  let hit = this.known_categories_by_id[id];
  if (hit) return hit;
  const they = yield app.categories.find(
    {_id: id}, {projection: _.cate_proj}
  );
  they.cdr.cancel();
  this.known_categories_by_id[id] = they.car
  return they.car
};
s.allCategories = function* () {
  while (! this.all_categories) {
    this.all_categories = yield app.categories.find(
      {}, {sort: 'depth_first_traversal_order', projection: _.cate_proj}
    );
    yield ['fork', this.populate_categories_by_id_from_all_categories()]
  };
  return this.all_categories;
};
s.populate_categories_by_id_from_all_categories = function* () {
  let rest = this.all_categories;
  while (rest.isCons) {
    const it = rest.car;
    this.known_categories_by_id[it._id] = it;
    rest = yield rest.cdr
  }
};


s.test = () => {
  globalThis.t = t;
};


t = {};

t.load = async function () {
  await app.mgr.find("app/cache.mjs");
  console.log("Loaded.");
};


t.clear = function () {
  t.succeeded = false;
  t.result = undefined;
  t.failed = false;
  t.reason = undefined
};
t.succeed = r => {
  t.succeeded = true;
  t.result = r;
  if (t.verbose) console.log("result", r);
};
t.fail = rea => {
  t.failed = true;
  t.reason = rea;
  if (t.verbose) console.error(rea);
};

t.tst0 = function () {
  /* An integration test between logical variables and the runner clan. */
  /* This test does not even involve the other code from this file. */
  t.tramp = lib.SyncTramp.new();
  t.ex0_teller = lib.lvar.new();
  t.ex0_teller.trampoline = t.tramp;
  t.ex0_teller.onDemand(() => t.ex0_teller.resolve("tst0"));
  t.ex1_runner = lib.Runner.clone();
  t.clear();
  t.ex1_runner.trampoline = t.tramp;
  t.ex1_runner.good = t.succeed;
  t.ex1_runner.bad = t.fail;
  t.ex1_runner.iterator = ( function* () {
    return yield t.ex0_teller.ask
  })();
  t.ex1_runner.resumeWith();
  t.tramp.exhaust();
  t.fail_msg = "Regression test failed.";
  if (! t.succeeded)       throw new Error(t.fail_msg);
  if (t.failed)            throw new Error(t.fail_msg);
  if ("tst0" !== t.result) throw new Error(t.fail_msg);
};

t.tst1 = function () {
  app.stdRunner.call(t.tst2)
};

t.tst2 = function* () {
  yield app.cache.allCategories();
  console.log("Done.");
};

t.query = function (again) {
  /* Call the given iterator and report what it says. */
  t.ctxt = app.stdRunner.clone();
  t.ctxt.good = t.succeed;
  t.ctxt.bad = t.fail;
  t.verbose = true;
  t.ctxt.iterator = again;
  t.ctxt.resumeWith();
};

// t.query(app.cache.categoryFromId(5))














