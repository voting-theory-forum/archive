let s = {};

export const asInstalled = async function ({mgr}) {
  app.normal    = Object.create(s.Priority);
  app.low       = Object.create(s.Priority);
  app.frivolous = Object.create(s.Priority);
  s.stackAbove(app.loading, app.normal);
  s.stackAbove(app.normal,     app.low);
  s.stackAbove(app.low,  app.frivolous);
}

s.Priority = {};
s.Priority.promise = function (f, ...args) {
  let res, rej;
  let r = new Promise( (arg_res, arg_rej) => {
    res = arg_res;
    rej = arg_rej
  });
  this.schedule( () => {
    f(...args).then(
      value  => {this.schedule(() => {res(value )})},
      reason => {this.schedule(() => {rej(reason)})}
    )
  });
  return r
};
s.Priority.ffTimeout = function (f, ms, ...args) {
  /*
    Fire-and-forget timeout. Takes the arguments that setTimeout would,
    but does not return a handle to cancel the timeout.
  */
  this.schedule( () => {
    setTimeout(
      () => {
        this.schedule(f, ...args)
      }, ms
    )
  })
}

s.Priority.enqueue = function (...args) {
  if (this.queuedTasks) this.queuedTasks.push(args)
  else this.queuedTasks = [args]
};
Object.defineProperty( s.Priority, 'hasAQueuedTask', {
  get: function () {
    return this.queuedTasks && this.queuedTasks.length > 0
  }, configurable: true, enumerable: true
});
s.Priority.execute = function (f, ...args) {
  this.isExecuting = true;
  f(...args);
  this.excite();
  this.isExecuting = false
};
Object.defineProperty( s.Priority, 'isBusy', {
  get: function () {
    return this.isExecuting ||
      this.hasQueuedTasks ||
      this.higher && this.higher.isBusy
  }, configurable: true, enumerable: true
});
s.Priority.schedule = function (...args) {
  if ( this.isExecuting || this.higher && this.higher.isBusy
  ) this.enqueue(...args);
  else this.execute(...args)
};
s.Priority.delayPeriod = 0;
s.Priority.excite = function () {
  if (! this.feelsExcited) {
    setTimeout(this.check, this.delayPeriod);
    this.feelsExcited = true
  }
};
Object.defineProperty( s.Priority, 'check', {
  get: function () {
    return this.check = ( function () {
      this.feelsExcited = false;
      /*
        If permitted to work, and have work, do some work.
        If permitted to work, but have no work, trigger the next lower level.
      */
      if ( ! this.isExecuting && ! this.higher        ||
           ! this.isExecuting && ! this.higher.isBusy
      ) { // permitted
        if (this.hasQueuedTasks) this.execute(this.queuedTasks.shift())
        else if (this.lower) this.lower.excite()
      } // outer 'if'
    }).bind(this)
  }, configurable: true, enumerable: true
});

s.stackAbove = (a, b) => {
  a.lower  = b;
  b.higher = a
};

/*
  t = {};
  t.ex = Object.create(s.Priority);
  t.ex.schedule(console.log, "foo");

  t.out_p = t.ex.promise(() => new Promise((res, rej) => {
    t.res = res;
    t.rej = rej
  }));
*/
