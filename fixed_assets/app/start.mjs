let s = {};

export const asInstalled = async function ({mgr}) {
  app.mgr = lib.mgr = mgr;
  await mgr.find("app/basic_page_index.mjs").catch(console.error);
  await mgr.find("lib/pgm.mjs"             ).catch(console.error);
  await mgr.find("lib/browser_utls.mjs"    ).catch(console.error);
  await mgr.find("app/std_runner.mjs"      ).catch(console.error);
  app.go = s.go;
  app.stdQuash = s.stdQuash;
  app.linkInternally = s.linkInternally;
  app._ = s;
  return {
    init:           s.init,
    go:             s.go,
    _:              s,
    stdQuash:       s.stdQuash,
    linkInternally: s.linkInternally
  }
};

s.stdQuash = () => {
  if (app.currentViewModule) {
    app.currentViewModule.stop()
  } else {
  }
  lib.emptyOut(app.mainBody)
};

s.linkInternally = function (...ids) { for (const id of ids) {
  let elt = typeof id === typeof "" ? document.getElementById(id) : id;
  elt.addEventListener('click', function (ev) {
    ev.preventDefault();
    app.go(elt.href).catch(console.error);
  });
}};

s.init = async function () {
  app.mainBody = document.getElementById('main-body');
  s.linkInternally('archive-button', 'banner');
  app.homepg_content = Array.from(app.mainBody.children);
  app.currentPage = {[0]: "home", title: "Voting Theory"};
  window.addEventListener('popstate', event => void app.go());
  app.go().catch(console.error);
};

/*
  Possible sources specifying destinations for setting view:
  - current location, changed by window popstate event;
  - current location, on startup;
  - href string, from the user clicking on something.
*/
s.go = async function (arg) {
  let t = {};
  const href = arg ? arg : location.pathname + location.search;
  if (typeof "" !== typeof href) throw new Error("WTF");
  t.tgt = app.basicPageIndex.abstract(href);
  if (! lib.deepEqual(app.currentPage, t.tgt)) {
    if (arg) history.pushState(t.tgt, t.tgt.title, href);
    let mod = await app.mgr.find(`views/${t.tgt[0]}.mjs`).catch(console.error);
    let r = mod.go(t.tgt);
    if (undefined !== r && null !== r && typeof {} === typeof r && r.next) {
      let runner = app.stdRunner.clone();
      runner.name = 'app/start/go'; /* dbg */
      runner.iterator = r;
      runner.good = () => {};
      runner.bad = console.error;
      runner.resumeWith()
    }
    app.currentViewModule = mod;
    app.currentPage = t.tgt
  }
};







