/*
  browser_utls -- Utilities to help do things you can do
  specifically in browsers, e. g. manipulating the DOM.
*/

let s = {};

export async function asInstalled () {
  let tags = {};
  [
    'div', 'p', 'a', 'span', 'table', 'tr', 'td', 'th', 'tbody', 'thead',
    'h1', 'h2', 'h3', 'h4', 'h5', 'h6', 'li', 'ul', 'ol', 'label'
  ].forEach(n => tags[n] = s.tag(n));
  /* const {
    div, p, a, span, table, tr, td, th, tbody, thead, h1, h2, h3, h4, h5, h6,
    li, ol, ul, label
  } = lib.tags; */
  let pub = {tags};
  ['emptyOut', 'tag'].forEach(n => pub[n] = s[n]);
  Object.assign(lib, pub);
  return pub
};


/* Trash the contents of one or more document elements. */
s.emptyOut = function (...parents) { for (let parent of parents) {
  let c;
  while (c = parent.firstChild) parent.removeChild(c);
}};

/*
  Want a more convenient way to compose document snippets than either HTML
  or the DOM. Use of HTML would require escaping variable contents, which is
  easy to get wrong and is a pain to do or think about.
  Direct manipulation of the DOM avoids that, but causes writer's cramp.
  So, provide here a way to say simple things like div(p("foo")) to mean
  <div><p>foo</p></div>.
*/
s.tag = type => (...args) => {
  let it = document.createElement(type);
  let attrs = {};
  let children_args = args;
  if (typeof {} === typeof args[0] && ! args[0].nodeType) {
    children_args = args.slice(1, args.length);
    attrs = args[0]
  };
  let hit;
  if (attrs.id) it.id = attrs.id;
  if (hit = attrs.classes) for (let each of hit) it.classList.add(each);
  if (hit = attrs.class) it.classList.add(hit);
  if (hit = attrs.href) it.href = hit;
  if (hit = attrs.value) it.value = hit;
  if (hit = attrs.style) for (const [k, v] of Object.entries(hit))
    it.style[k] = v;
  children_args.forEach( each => {
    let cand = each;
    if (typeof "" === typeof each) cand = document.createTextNode(each);
    else if (typeof 3 === typeof each)
      cand = document.createTextNode("" + each);
    it.appendChild(cand)
  });
  return it
}
