/*
  There are now distinct versions of module_manager.mjs
  for the server and for the code that goes to the browser.
  A reader said they were using Waterfox Classic browser and
  it was working with NodeBB. I determined that whenever I
  happen to hear about a browser that works with NodeBB, I
  would try to support it when serving out the home page and
  the archive, too. Waterfox Classic does not support the
  dynamic "import(...)" construct. So the present version
  of the module manager works around this. The present
  version definitely will not work in Node.
*/

const our_import = async function (full_path) {
  const x = this.next_index++;
  let le_cas = this.tmp[x] = {};
  le_cas.done = new Promise( (res, rej) => {
    Object.assign(le_cas, {res, rej})
  });
  let elt = document.createElement('script');
  elt.setAttribute('type', 'module');
  elt.innerHTML = `
    let le_cas = app.mgr.tmp[${x}];
    import * as its_exports from "${full_path}";
    le_cas.res(its_exports)
  `;
  document.body.appendChild(elt);
  const its_exports = await le_cas.done;
  elt.remove();
  this.tmp[x] = undefined;
  return its_exports
};

const load$ = function (bare_path) {
  // Private procedure.
  console.log(`load ${bare_path}`);
  let full_path = "/" + bare_path;
  this.modules_by_bare_path[bare_path] = 
    this.our_import(full_path).then( exports => {
      return exports.asInstalled ?
        exports.asInstalled({
          mgr: this,
        }) : exports
    });
  return undefined
};

const find = function (bare_path) {
  let hit;
  while (! (hit = this.modules_by_bare_path[bare_path]))
    this.load$(bare_path);
  return hit  // is a promise
};

const asAlreadyInstalled = function () {
  return app.mgr && app.mgr.find === find ?
    app.mgr : undefined;
};
const install = function () {
  let mgr = Object.create({load$, find, our_import});
  mgr.modules_by_bare_path = {};
  mgr.next_index = 0;
  mgr.tmp = {};
  app.mgr = mgr;
  return mgr
};
const asInstalled = function () {
  let hit = asAlreadyInstalled();
  return hit ? hit : install();
};

export {asInstalled}
