/*
  manual_ops -- manual operations

  Try to support occasional manual operations using the
  REPL, with relevant pieces of the application available
  to be called.

  This is up here (rather than tucked into a subdirectory) so that the
  module manager will start from the correct context.

  node --trace-warnings
  require("./manual_ops");
  delete require.cache[require.resolve("./manual_ops")];

*/

globalThis.app || (globalThis.app = {});
globalThis.lib || (globalThis.lib = {});
lib.require = require;

( async function () {
  app.mgr = (await import("./module_manager.mjs")).init();
})();

/* ( async function () {
  app.mgr = (await import("./module_manager.mjs")).init();
  require("envelop/manual_ops/traverse_categories.js");
})(); */

