let List = {beh: {}};
List.new = function () {return Object.create(this.beh)};
List.new_subclass = function () {
  let it = Object.create(this);
  it.beh = Object.create(this.beh);
  return it
};
let Cons = List.new_subclass();
let Nil  = List.new_subclass();
List.beh.isList = true;
Nil. beh.isNil  = true;
Cons.beh.isCons = true;
const nil = Nil.beh;
export {Cons, nil}
