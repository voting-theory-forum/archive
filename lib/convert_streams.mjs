let s = {};

export async function asInstalled (env) {
  try { await Promise.all([
    s.import_lvar(env), s.import_list(env), s.import_prg(env)
  ])} catch (err) {
    console.error(err);
    return Promise.reject(err)
  };
  let exports = {};
  ['listFromAsyncIterable'].forEach( name => {
    lib[name] = exports[name] = s[name];
  });
  return exports
};
s.import_list = async function ({mgr}) {
  try {
    lib.list = await mgr.find("lib/list.mjs")
  } catch (err) {
    console.error(err);
    return Promise.reject(err)
  }
};
s.import_lvar = async function ({mgr}) {
  try {
    await mgr.find("lib/list.mjs") // self installs as globalThis.lib.list.
  } catch (err) {
    console.error(err);
    return Promise.reject(err)
  }
};
s.import_prg = async function ({mgr}) {
  try {
    await mgr.find("lib/pgm.mjs") // Installs each utility in globalThis.lib.
  } catch (err) {
    console.error(err);
    return Promise.reject(err)
  }
};


s.listFromAsyncIterable = it => {
  let ribo = Object.create(s.Ribo);
  ribo.huah       = s.Ribo.huah      .bind(ribo);
  ribo.groundNext = s.Ribo.groundNext.bind(ribo);
  ribo.src = it[Symbol.asyncIterator]();
  ribo.dst = lib.lvar.new();
  ribo.dst.onDemand(ribo.huah);
  return ribo.dst.ask
};
s.listFromSyncIterable = it => {
  let ribo = Object.create(s.SyncRibo);
  ribo.huah       = s.Ribo.huah      .bind(ribo);
  ribo.src = it[Symbol.iterator]();
  ribo.dst = lib.lvar.new();
  ribo.dst.onDemand(ribo.huah);
  return ribo.dst.ask
};

s.Ribo = {};
s.Ribo.huah = function () {
  const {ohPromiseMe} = lib;
  ohPromiseMe(console.error)(this.src.next.bind(this.src))(this.groundNext)
};
s.Ribo.groundNext = function ({value, done}) {
  const {list, lvar} = lib;
  if (done) this.dst.resolve(list.nil); else {
    let a_cons = list.Cons.new();
    a_cons.car = value;
    new_dst = lib.lvar.new();
    a_cons.cdr = new_dst.ask;
    this.dst.resolve(a_cons);
    new_src = this.src; // has mutated.
    // Finally mutate self to take the role of ribosome for the next paring of
    // input element with corresponding output element.
    this.src = new_src;
    this.dst = new_dst;
    this.dst.onDemand(this.huah)
  }
};

s.SyncRibo = {};
s.SyncRibo.huah = function () {
  const {list, lvar} = lib;
  const {value, done} = this.src.next(); // mutates src
  if (done) this.dst.resolve(list.nil); else {
    let a_cons = list.Cons.new();
    a_cons.car = value;
    new_dst = lib.lvar.new();
    a_cons.cdr = new_dst.ask;
    this.dst.resolve(a_cons);
    new_src = this.src; // has mutated.
    // Finally mutate self to take the role of ribosome for the next pairing of
    // input element with corresponding output element.
    this.src = new_src;
    this.dst = new_dst;
    this.dst.onDemand(this.huah)
  }
}

