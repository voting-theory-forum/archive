// lvar -- Logical variables

let s = {}; // script.

export const asInstalled = async function ({mgr}) {
  try {
    await mgr.find("lib/pgm.mjs");
  } catch (err) {
    console.log(err);
    return Promise.reject(err);
  };
  return lib.lvar = {
    new:     () => s.new_pvt().tell,  // fundamental
    all:     s.all,                   // combinator
    allBind: s.allBind,               // combinator
    _:       s,                       // debugging/testing
    bindToPromiseProc: s.bindToPromiseProc, // more combinators.
    fromPromiseProc: s.fromPromiseProc
  }
};

s.DefaultTrampoline = {
  defer: function (f, ...args) { setTimeout(() => f(...args)) }
}


// For internal use, we need this primitive event library.

s.Ev = {beh: {}};
s.Ev.new = () => {
  let it = Object.create(s.Ev.beh);
  it.subscribers = [];
  it.trampoline = s.DefaultTrampoline;
  return it
}
s.Ev.beh.subscribe = function (cb) {
  if (this.hasBeenCaused) this.trampoline.defer(cb, this.value)
  else this.subscribers.push(cb);
};
s.Ev.beh.cause = function (value) {
  this.hasBeenCaused = true;
  this.value = value;
  for (const e of this.subscribers)
    this.trampoline.defer(e, value)
  this.subscribers = []
}
s.Ev.beh.stifle = function () {
  this.cause       = ()  => {};
  this.subscribe   = ()  => {};
  this.value       = undefined;
  this.subscribers = undefined
};


// Implement lvar.

s.ev_names = ['cancel', 'demand', 'resolve', 'reject'];
s.Pvt = {
  get trampoline () {return this._trampoline},
  set trampoline (newpri) {
    this._trampoline = newpri;
    for (const k of s.ev_names) this[k].trampoline = newpri
  }
};
s.new_pvt = function () {
  /*
    new private -- create and return a new private object to function as the
    center of the implementation of a logical variable. Access by clients is
    insulated by two interfaces, the asking interface and the telling
    interface. These are mediated by dedicated objects.
  */
  let it = Object.create(s.Pvt);
  s.ev_names.forEach( n => {
    it[n] = s.Ev.new();
    it[n].name = n; // debugging
    Object.defineProperty( it[n], 'ownerName', { // dbg
      configurable: true,
      enumerable: true,
      get: () => it.tell.name
    })
  });
  it.trampoline = s.DefaultTrampoline;
  it.tell = it.teller = {};
  it.tell.ask = it.tell.asker = {};
  for (const n in s.tell_methods)    it.tell[n] = s.tell_methods[n].bind(it);
  for (const n in s.ask_methods) {
    it.tell.ask[n] = s.ask_methods [n].bind(it)
  };
  const ifc_set_pri = newpri => {it.trampoline = newpri};
  for (const ifc of [it.tell, it.tell.ask]) 
    Object.defineProperty( ifc, 'trampoline', {
      configurable: true, enumerable: true, set: ifc_set_pri
    });
  it.tell.ask.isLogicalVariable = true;
  Object.defineProperty( it.tell, 'name', { // for debugging
    configurable: true,
    enumerable: true,
    set: n => it.name = n,
    get: () => it.name || ""
  });
  it.tell._ = it; // For debugging.
  return it
};
s.ask_methods = {
  dereference: function (rejf) { return resf => {
    this.resolve.subscribe(resf);
    this.reject .subscribe(rejf);
    this.demand .cause(true)
  }},
  cancel: function () {
    this.cancel.cause(true);
    this.demand .stifle();
    this.resolve.stifle();
    this.reject .stifle();
  }
};
s.tell_methods = {
  onDemand: function (then_what) {
    this.demand.subscribe( () => {
      this.demand.stifle();
      this.trampoline.defer(then_what);
    });
  },
  onCancel: function (f) {
    this.cancel.subscribe(f);
  },
  reject: function (reason) {
    this.reject.cause(reason);
    this.cancel .stifle();
    this.resolve.stifle();
    this.demand .stifle()
  },
  resolve: function (value) {
    this.resolve.cause(value)
    this.cancel.stifle();
    this.reject.stifle();
    this.demand.stifle()
  }
};

s.ask_methods.next = function () {
  /* Make any clone of Runner think we are an iterator. */
  let ask = this.tell.ask; /* not what your country can do for you */
  let manip = runner => ask.dereference(runner.bad)(runner.good);
  let genf = function* () {
    yield ['reflect', manip]
  };
  let again = genf();
  return again.next()
};

// Manual testing.

s.test = () => {
  let t = globalThis.t = {};
  globalThis.s = lib.lvar._;

  t.ex0_tell = lib.lvar.new();
  t.ex0_ask  = t.ex0_tell.ask;

  t.good = v => console.log("value", v);
  t.bad  = r => console.log("bad", r);
};


// Combinators. Should they be here, or in a file or files of their own?

s.all = (...ins) => {
  let out = lib.lvar.new();
  out.name = "all"; // debugging
  return s.allBind({ins, out})
};
s.allBind = ({ins, out}) => {
  let o = Object.create(s.AllAccumulator);
  o.args = ins;
  o.out = out;
  o.out.onDemand(o.start.bind(o));
  return o.out.ask
};

s.AllAccumulator = {
  start: function () {
    this.outst_ct =          this.args.length;
    this.results = new Array(this.args.length);
    this.reject = this.reject.bind(this);
    for (const [x, a] of this.args.entries())
      a.dereference(this.reject)(v => this.resolve(x, v));
    this.ck()
  },
  reject: function (why) {
    this.args.forEach(a => a.cancel());
    this.out.reject(why);
    this.stifle()
  },
  resolve: function (index, result) {
    this.results[index] = result;
    --this.outst_ct;
    this.ck()
  },
  ck: function () { if (this.outst_ct <= 0) {
    this.out.resolve(this.results);
    this.stifle();
  }},
  stifle: function () {
    this.args     = undefined;
    this.out      = undefined;
    this.outst_ct = undefined;
    this.results  = undefined;
    this.reject   = ()  => {};
    this.resolve  = ()  => {}
  }
};

s.bindToPromiseProc = ({out, promiseProc}) => {
  // Given a teller, bind it to a JS "function" that when called without any
  // args, returns a promise.
  out.onDemand = () => lib.ohPromiseMe(out.reject)(promiseProc)(out.resolve);
  return out.ask
};
s.fromPromiseProc = promiseProc => {
  let out = lib.lvar.new();
  out.name = "fromPromise"; // dgb
  return s.bindToPromiseProc({out, promiseProc})
}
