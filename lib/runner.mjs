let s, t;

export const asInstalled = async function ({mgr}) {
  s.discipline(s.Runner, s.Runner.methods);
  lib.Runner = s.Runner;
  lib.SyncTramp = s.SyncTramp; /* Used for testing. */
  return lib
}

s = {};

s.discipline = function (discipulum, magistrō) {
  for (const k in magistrō) discipulum[k] = magistrō[k].bind(discipulum);
  return discipulum
};

s.Runner = {
  clonable_attribute_names: [
    'clonable_attribute_names',
    'methods',
    'good',           /* Procedure to call with my returned value if I
                         succeed. */
    'bad',            /* Procedure to call with the reason if I fail. */
    'trampoline'      /* basis of deferral of actions (sync or async). */
  ],
  methods: {},
  good: v  => console.log("returned", v),
  bad: console.error,
  trampoline: {defer: () => {throw new Error("no default trampoline")}},
};

s.Runner.clone = s.Runner.methods.clone = function () {
  let c = {};
  s.discipline(c, this.methods);
  for (const k of this.clonable_attribute_names) c[k] = this[k];
  c.interpreter = Object.create(s.Runner.Interpreter);
  c.interpreter.runner = c;
  return c
};

s.Runner.methods.resumeWith = function (passing_in) {
  const yo = this.trampoline.defer;
  const hit = this.iterator.next(passing_in);
  const passed_out = hit.value;
  if (hit.done) yo(this.good, passed_out);    /* return ... */
  else this.interpreter.interpret(passed_out) /* yield  ... */
};

s.Runner.methods.call3 = function (bad) { return again => good => {
  let c = this.clone();
  c.bad = bad;
  c.good = good;
  c.iterator = again;
  this.trampoline.defer(c.resumeWith)
}}

s.Runner.Interpreter = {};
s.Runner.Interpreter.reset = function () {
  this.name = "";
  this.pc = undefined; /* program counter */
  this.good = this.runner.resumeWith;
  this.bad = this.runner.bad
};
s.Runner.Interpreter.interpret = function (input) {
  this.reset();
  this.pc = input.next ? [input] : input; /* Looks like an iterator? */
  if (undefined === this.pc.length) throw new Error(
    `${this.name}: ${pc}: ill-formed command yielded.`
  );
  while (this.pc) this.step();
};
s.Runner.Interpreter.step = function () {
  /* step -- private -- execute one step of interpreting the command. */
  if (this.pc[0].next) this.interpret_iterator();
  else switch (this.pc[0]) {
  case false:     this.interpret_false(); break;
  case 'fork':    this.fork();            break;
  case 'reflect': this.reflect();         break;
  case 'name':    this.set_name();        break;
  case 'bad':     this.set_bad();         break;
  default:
    throw new Error(`${this.name}: ${pc}: unrecognized command.`)
  }
};
s.Runner.Interpreter.interpret_iterator = function () {
  /* Private -- interpret the circumstance where we see an iterator as
     the next command. Generally, this means we will call the iterator. */
  const yo = this.runner.trampoline.defer; /* Demeter weeps */
  let next_context = this.runner.clone();
  next_context.good = this.good;
  next_context.bad = this.bad;
  next_context.callingRunner = this.runner;
  next_context.name = this.name;
  next_context.iterator = this.pc[0];
  yo(next_context.resumeWith);
  this.pc = undefined
};
s.Runner.Interpreter.interpret_false = function () {
  /* Private -- interpret the circumstance where the next verb is exactly
     the constant `false`. We will interpret it as a command to fail out
     along the unhappy path. */
  const yo = this.runner.trampoline.defer;
  yo(this.runner.bad, this.pc[1]);
  this.pc = undefined
};
s.Runner.Interpreter.fork = function () {
  const yo = this.runner.trampoline.defer;
  this.pc.shift();
  this.good = () => {
    console.log("Forked thread finished successfully");
  };
  let name = this.name;
  this.bad = reason => {
    console.log(`${name}: Forked thread: ${reason}`);
  };
  yo(this.runner.resumeWith);
};
s.Runner.Interpreter.reflect = function () {
  const yo = this.runner.trampoline.defer;
  const action = this.pc[1];
  this.pc = undefined
  yo(action, this.runner);
  /* Leave it to the action as to whether to resume or not. */
};
s.Runner.Interpreter.set_name = function () {
  /* For debugging. */
  this.pc.shift(); /* the verb, 'name' */
  this.name = this.pc.shift();
};
s.Runner.Interpreter.set_bad = function () {
  this.pc.shift();
  this.bad = this.pc.shift();
}

/*
  Have we already put all the methods we want in `s.Runner.methods`?
*/
s.discipline(s.Runner, s.Runner.methods);

/*
  For testing, we need a way to know when there are no more deferred actions.
  A synchronous trampoline can achieve that.
*/
s.SyncTramp = {methods: {}};
s.SyncTramp.new = function () {
  let n = {};
  s.discipline(n, this.methods);
  n.queue = [];
  return n
};

s.SyncTramp.methods.defer = function (...args) {
  this.queue.push(args)
};

s.SyncTramp.methods.exhaust = function () { while (this.queue.length) {
  const task = this.queue.shift();
  task[0](...task.slice(1))
}};


t = {};

t.tramp = s.SyncTramp.new()
t.ex0 = function* (arg) {
  return `0(${arg})`
};

t.ex1 = function* (arg) {
  const ant = `inner1(${arg})`;
  return `outer1(${yield t.ex0(ant)})`
};
t.clear = function () {
  t.succeeded = false;
  t.result = undefined;
  t.failed = false;
  t.reason = undefined
};
t.succeed = r => {
  t.succeeded = true;
  t.result = r;
};
t.fail = rea => {
  t.failed = true;
  t.reason = rea
};
t.runner = s.Runner.clone()
t.runner.trampoline = t.tramp;
t.runner.good = t.succeed;
t.runner.bad = t.fail;
t.runner.iterator = t.ex1("arg");
t.clear();
t.runner.resumeWith();
t.tramp.exhaust();
if (! t.succeeded) throw new Error("Regression test failed.");
if (t.failed) throw new Error("Regression test failed.");
if ("outer1(0(inner1(arg)))" != t.result)
  throw new Error("Regression test failed.");


