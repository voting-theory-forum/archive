/* pgm --- proramming utilities */

let s = {}; /* script */

export const asInstalled = async function () {
  Object.assign(lib, s);
  return s
};

/*
  Play a "generator function" so we can
  use it as an asyncrhronous
  coroutine (like async function, but without an opinion in favor of Promise).
  Each "yield" should be handed a procedure that takes another procedure as an
  argument and the first procedure is to call the second procedure with its
  result. This result comes back as the result of the "yield."
*/

s.play = genf => s.run(genf());
s.run = again => {
  let cont, b;
  b = proc => setTimeout(() => proc(cont), 0)
  cont = arg => {
    const hit = again.next(arg);
    if (! hit.done) b(hit.value);
  };
  cont('ignored')
};

/* try recasting the above with changes in the details of the calls,
// for convenience of use
*/
s.to = genf => bad => (...spec) => good => {
  const ugly = function (reason) {
    setTimeout(() => bad(reason));
    return cb => {}
  };
  let again = genf(ugly, ...spec);
  const cont = yv => {
    const hit = again.next(yv);
    const v = hit.value;
    setTimeout(hit.done ? () => good(v) : () => v(cont))
  };
  cont()
};
/* Test!
  s = {};
  Paste in s.to definition from above.
  t = {};
  t.ex0 = s.to( function* (bad, ...spec) {
    console.log(...spec);
    yield bad("to the bone");
    return 2
  });
  t.dislike = reason => console.error("Bad:", reason);
  t.like = r => console.log("Result:", r);
  t.ex0(t.dislike)('this', 'is', 'a', 'test')(t.like);
  t.ex1 = s.to( function* (bad, ...spec) {
    console.log(...spec);
    return `Exactly ${yield cb => cb(2)}`
  });
  t.ex1(t.dislike)('this', 'is', 'a', 'test')(t.like);

  Try nesting calls to these things.

  t.ex2 = s.to( function* (bad) {
    yield t.ex3(bad)();
  });
  t.ex3 = s.to( function* (bad) {
    yield bad("to the bone")
  });
  t.ex2(t.dislike)()(t.like)
*/

s.ohPromiseMe = sad => f => async function (happy) {
  try {
    happy(await f())
  } catch (err) {
    sad(err)
  }
};

s.deepEqual = (a, b) => {
  if (a === b) return true;
  if (typeof a !== typeof b) return false;
  /* Assume will not encounter NaN. */
  if (typeof {} != typeof a) return false;
  if (null === a || null === b) return false;
  if (a[Symbol.iterator] && b[Symbol.iterator]) {
    let bi = b[Symbol.iterator]();
    for (const ae of a) {
      let bir = bi.next();
      if (bir.done || ! s.deepEqual(bir.value, ae)) return false;
    };
    return bi.next().done
  };
  const sort_on_keys = x => x.sort(
    (a, b) => a[0] < b[0] ? -1 : a[0] > b[0] ? 1 : 0
  );
  return s.deepEqual(
    sort_on_keys(Object.entries(a)), sort_on_keys(Object.entries(b))
  )
}

